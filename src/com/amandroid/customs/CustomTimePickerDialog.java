package com.amandroid.customs;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class CustomTimePickerDialog extends TimePickerDialog {

    private final static int TIME_PICKER_INTERVAL = 15;
    public TimePicker timePicker;
    private final OnTimeSetListener callback;
    private final OnTimeSetListener callbackAucun;

    public CustomTimePickerDialog(Context context, OnTimeSetListener callBack, OnTimeSetListener callBackAucun,
                                  int hourOfDay, int minute, boolean is24HourView) {
        super(context, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL,
                is24HourView);
        this.callback = callBack;
        this.callbackAucun = callBackAucun;
        // Set negative button click listener
        this.setButton(DialogInterface.BUTTON_NEGATIVE, "Aucun",this);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
    	//On verifie si on a cliqué sur le bouton aucun 
    	if(which==-2 && callbackAucun != null && timePicker != null){
    		timePicker.clearFocus();
            callbackAucun.onTimeSet(timePicker, timePicker.getCurrentHour(),
                    timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);
    		
    	}else{
			if (callback != null && timePicker != null) {
				timePicker.clearFocus();
				callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
				        timePicker.getCurrentMinute() * TIME_PICKER_INTERVAL);
			}
    	}
    	
       
    }

    @Override
    protected void onStop() {
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            Field timePickerField = classForid.getField("timePicker");
            this.timePicker = (TimePicker) findViewById(timePickerField
                    .getInt(null));
            Field field = classForid.getField("minute");

            NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));
            mMinuteSpinner.setMinValue(0);
            mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
            List<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            mMinuteSpinner.setDisplayedValues(displayedValues
                    .toArray(new String[0]));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}