package com.amandroid.models;

import java.io.Serializable;

public class Fiche implements Serializable {

	  private String jour;
	  private long chanter_id;
	  private long chef_id;
	  private long employe_id;
	  private String matin_deb;
	  private String matin_fin;
	  private String aprem_deb;
	  private String aprem_fin;
	  private boolean trajet;
	  private boolean repas;
	  private boolean gd;
	
	public Fiche(){}
	  
	public Fiche(String jour, long chanter_id, long chef_id, long employe_id,
			String matin_deb, String matin_fin, String aprem_deb,
			String aprem_fin, boolean trajet, boolean repas, boolean gd) {
		
		this.jour = jour;
		this.chanter_id = chanter_id;
		this.chef_id = chef_id;
		this.employe_id = employe_id;
		this.matin_deb = matin_deb;
		this.matin_fin = matin_fin;
		this.aprem_deb = aprem_deb;
		this.aprem_fin = aprem_fin;
		this.trajet = trajet;
		this.repas = repas;
		this.gd = gd;
	}
	
	public String getJour() {
		return jour;
	}
	public void setJour(String jour) {
		this.jour = jour;
	}
	public long getChanter_id() {
		return chanter_id;
	}
	public void setChanter_id(long chanter_id) {
		this.chanter_id = chanter_id;
	}
	public long getChef_id() {
		return chef_id;
	}
	public void setChef_id(long chef_id) {
		this.chef_id = chef_id;
	}
	public long getEmploye_id() {
		return employe_id;
	}
	public void setEmploye_id(long employe_id) {
		this.employe_id = employe_id;
	}
	public String getMatin_deb() {
		return matin_deb;
	}
	public void setMatin_deb(String matin_deb) {
		this.matin_deb = matin_deb;
	}
	public String getMatin_fin() {
		return matin_fin;
	}
	public void setMatin_fin(String matin_fin) {
		this.matin_fin = matin_fin;
	}
	public String getAprem_deb() {
		return aprem_deb;
	}
	public void setAprem_deb(String aprem_deb) {
		this.aprem_deb = aprem_deb;
	}
	public String getAprem_fin() {
		return aprem_fin;
	}
	public void setAprem_fin(String aprem_fin) {
		this.aprem_fin = aprem_fin;
	}
	public boolean isTrajet() {
		return trajet;
	}
	public void setTrajet(boolean trajet) {
		this.trajet = trajet;
	}
	public boolean isRepas() {
		return repas;
	}
	public void setRepas(boolean repas) {
		this.repas = repas;
	}
	public boolean isGd() {
		return gd;
	}
	public void setGd(boolean gd) {
		this.gd = gd;
	}
	
	@Override
	public String toString() {
		return "Fiche [jour=" + jour + ", chanter_id=" + chanter_id
				+ ", chef_id=" + chef_id + ", employe_id=" + employe_id
				+ ", matin_deb=" + matin_deb + ", matin_fin=" + matin_fin
				+ ", aprem_deb=" + aprem_deb + ", aprem_fin=" + aprem_fin
				+ ", trajet=" + trajet + ", repas=" + repas + ", gd=" + gd
				+ "]";
	}
	  
}
