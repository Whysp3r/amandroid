package com.amandroid.models;

import java.io.Serializable;

public class Worker implements Serializable {

	  private String name;
	  private long id;
	  private boolean selected;

	  public Worker(String name, long id) {
	    this.name = name;
	    this.id = id;
	    selected = false;
	  }
	  
	  public Worker(String name) {
		    this.name = name;
	  }

	  public String getName() {
	    return name;
	  }
	  public void setName(String name) {
		 this.name = name;
	  }
	  
	  public long getId(){
		  return id;
	  }
	  
	  public void setId(long id){
		  this.id=id;
	  }

	  public boolean isSelected() {
	    return selected;
	  }

	  public void setSelected(boolean selected) {
	    this.selected = selected;
	  }

	@Override
	public String toString() {
		return "Worker [name=" + name + ", id=" + id + ", selected=" + selected
				+ "]";
	}
	  
	} 