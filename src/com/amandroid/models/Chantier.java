package com.amandroid.models;

public class Chantier {

	private int id;
	private String nom;
	private String num;
	private String id_mysql;
	
	public Chantier(){}
	
	public Chantier(String num, String nom, String id_mysql){
		this.nom = nom;
		this.num = num;
		this.id_mysql = id_mysql;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getId_mysql() {
		return id_mysql;
	}

	public void setId_mysql(String id_mysql) {
		this.id_mysql = id_mysql;
	}

	@Override
	public String toString() {
		return "Chantier [id=" + id + ", nom=" + nom + ", num=" + num
				+ ", id_mysql=" + id_mysql + "]";
	}


	
	
	
	

}
