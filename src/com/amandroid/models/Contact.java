/**
 * Created by Whysper on 04/12/2014.
 */

package com.amandroid.models;

public class Contact {

    private int id;
    private String prenom;
    private String nom;
    private String id_mysql;
    private String tel;
    private String add;
    private String ville;
    private String commentaire;
    private String statut;
    private String email;

    public Contact(){}

    public Contact(String prenom, String nom, String id_mysql) {
        this.prenom = prenom;
        this.nom = nom;
        this.id_mysql = id_mysql;
    }

    public Contact(String prenom, String nom, String id_mysql,
                   String tel, String add, String ville, String commentaire,
                   String statut, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.id_mysql = id_mysql;
        this.tel = tel;
        this.add = add;
        this.ville = ville;
        this.commentaire = commentaire;
        this.statut = statut;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getId_mysql() {
        return id_mysql;
    }

    public void setId_mysql(String id_mysql) {
        this.id_mysql = id_mysql;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Interim [id=" + id + ", prenom=" + prenom + ", nom=" + nom
                + ", id_mysql=" + id_mysql + ", tel=" + tel + ", add=" + add
                + ", ville=" + ville + ", commentaire=" + commentaire
                + ", statut=" + statut + "]";
    }
}


