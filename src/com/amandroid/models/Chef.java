package com.amandroid.models;

public class Chef {

	private int id;
	private String login;
	private String pass;
	private String id_mysql;
	
	public Chef(){}
	
	public Chef(String log, String pass, String id_mysql){
		this.login = log;
		this.pass = pass;
		this.id_mysql = id_mysql;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getId_mysql() {
		return id_mysql;
	}

	public void setId_mysql(String id_mysql) {
		this.id_mysql = id_mysql;
	}

	@Override
	public String toString() {
		return "Chef [id=" + id + ", login=" + login + ", pass=" + pass
				+ ", id_mysql=" + id_mysql + "]";
	}
	
	

}
