package com.amandroid.models;

public class DetailsFicheItem {
	public String nom;
	public String prenom;
	public String nomPrenom;
	public String matin_debut;
	public String matin_fin;
	public String aprem_debut;
	public String aprem_fin;
	public Boolean repas;
	public Boolean granddeplacement;
	public Boolean trajet;
	
	public DetailsFicheItem(){
		nom = "";
		prenom = "";
		matin_debut = "";
		matin_fin = "";
		aprem_debut = "";
		aprem_fin = "";
		repas = false;
		granddeplacement = false;
		trajet = false;
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNomPrenom(){
		return this.nomPrenom;
	}
	
	public void setNomPrenom(String nomPrenom) {
		this.nomPrenom = nomPrenom;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMatin_debut() {
		return matin_debut;
	}

	public void setMatin_debut(String matin_debut) {
		this.matin_debut = matin_debut;
	}

	public String getMatin_fin() {
		return matin_fin;
	}

	public void setMatin_fin(String matin_fin) {
		this.matin_fin = matin_fin;
	}

	public String getAprem_debut() {
		return aprem_debut;
	}

	public void setAprem_debut(String aprem_debut) {
		this.aprem_debut = aprem_debut;
	}

	public String getAprem_fin() {
		return aprem_fin;
	}

	public void setAprem_fin(String aprem_fin) {
		this.aprem_fin = aprem_fin;
	}

	public Boolean getRepas() {
		return repas;
	}

	public void setRepas(Boolean repas) {
		this.repas = repas;
	}

	public Boolean getGranddeplacement() {
		return granddeplacement;
	}

	public void setGranddeplacement(Boolean granddeplacement) {
		this.granddeplacement = granddeplacement;
	}

	public Boolean getTrajet() {
		return trajet;
	}

	public void setTrajet(Boolean trajet) {
		this.trajet = trajet;
	}
	
	
	
}
