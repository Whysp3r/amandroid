package com.amandroid.models;

public class DFiche {
	
	private int id;
	private String jour;
	private String id_chant;
	private String id_chef;
	
	public DFiche(){}
	
	public DFiche(String jour, String id_chant, String id_chef){
		this.jour = jour;
		this.id_chant = id_chant;
		this.id_chef = id_chef;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public String getId_chant() {
		return id_chant;
	}

	public void setId_chant(String id_chant) {
		this.id_chant = id_chant;
	}

	public String getId_chef() {
		return id_chef;
	}

	public void setId_chef(String id_chef) {
		this.id_chef = id_chef;
	}

	@Override
	public String toString() {
		return "DFiche [id=" + id + ", jour=" + jour + ", id_chant=" + id_chant
				+ ", id_chef=" + id_chef + "]";
	}
	
}
