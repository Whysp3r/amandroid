package com.amandroid.async_task;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
 
import com.amandroid.activies.RecapFicheActivity;
import com.amandroid.models.Fiche;
import com.amandroid.serializables.FicheSerializable;
import com.amandroid.serializers.FicheSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
 
public class JSONTransmitter extends AsyncTask<JSONObject, JSONObject, JSONObject> {
 
    String url = "http://www.alpex-epdm.fr/AM/api/setFicheFromAndroid";
      
    @Override
    protected JSONObject doInBackground(JSONObject... data) {
        JSONObject json = data[0];
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
 
        JSONObject jsonResponse = null;
        HttpPost post = new HttpPost(url);
        try {
         
            StringEntity se = new StringEntity("json="+json);
            
            post.addHeader("content-type", "application/x-www-form-urlencoded");           
            post.setEntity(se);
            
            HttpResponse response;
            response = client.execute(post);
            String resFromServer = EntityUtils.toString(response.getEntity());
            System.out.println("resFromServer: "+resFromServer);
            jsonResponse =new JSONObject(resFromServer);

            
        } catch (Exception e) { e.printStackTrace();}
          
        return jsonResponse;
    } 
}
