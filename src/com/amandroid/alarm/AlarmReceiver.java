package com.amandroid.alarm;


import com.amandroid.activies.LoginActivity;

import com.amandroid.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver{
	
	private NotificationManager mManager;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {		
			mManager = (NotificationManager) context.getApplicationContext().getSystemService(context.getApplicationContext().NOTIFICATION_SERVICE);
			Intent intent1 = new Intent(context.getApplicationContext(),LoginActivity.class);
			 
			Notification notification = new Notification(R.drawable.ic_launcher,"Avenir Metal Rappel", System.currentTimeMillis());
			intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 
			PendingIntent pendingNotificationIntent = PendingIntent.getActivity( context.getApplicationContext(),0, intent1,PendingIntent.FLAG_UPDATE_CURRENT);
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
			notification.setLatestEventInfo(context.getApplicationContext(), "Avenir Metal", "Remplissez les fiches d'heures", pendingNotificationIntent);
			 
			mManager.notify(0, notification);
		} catch (Exception r) {
			Toast.makeText(context, "Erreur.",Toast.LENGTH_SHORT).show();
			r.printStackTrace();
		}		
	}
}
