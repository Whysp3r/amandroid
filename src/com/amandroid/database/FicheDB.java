package com.amandroid.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amandroid.models.Chef;
import com.amandroid.models.DFiche;
import com.amandroid.models.Employe;
import com.amandroid.models.Fiche;

public class FicheDB {
	
	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	
	private static final String TABLE_FICHE = "table_fiche";
	private static final String FIC_ID = "ID";
	private static final String FIC_JOUR = "Jour";
	private static final String FIC_CHEF_ID = "Chef_id";
	private static final String FIC_CHAN_ID = "Chantier_id";
	private static final String FIC_EMP_ID = "Employe_id";
	private static final String FIC_MAT_DEB = "Matin_debut";
	private static final String FIC_MAT_FIN = "Matin_fin";
	private static final String FIC_APR_DEB = "Aprem_debut";
	private static final String FIC_APR_FIN = "Aprem_fin";
	private static final String FIC_TRAJET = "Trajet";
	private static final String FIC_REPAS = "Repas";
	private static final String FIC_GD = "Grand_Deplacement";

	
	private String[] allColumns = {FIC_ID, FIC_JOUR, FIC_CHEF_ID, FIC_CHAN_ID, FIC_EMP_ID,
									FIC_MAT_DEB, FIC_MAT_FIN, FIC_APR_DEB, FIC_APR_FIN, 
									FIC_TRAJET, FIC_REPAS, FIC_GD };
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public FicheDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}

	public int deleteAll(){
		return this.bdd.delete(TABLE_FICHE, null, null);
	}

	public int deleteByJourChantierEmploye(String jour, long idChantier, long idEmploye){
		String where =  FIC_JOUR+"=?"
						+" and "+FIC_CHAN_ID+"=?"
						+" and "+FIC_EMP_ID+"=?";
		return this.bdd.delete(TABLE_FICHE, where, new String[]{jour,String.valueOf(idChantier),String.valueOf(idEmploye)});
	}
 
	public long addFiche(Fiche fiche){
		
		ContentValues values = new ContentValues();
		
		values.put(FIC_JOUR, fiche.getJour());
		values.put(FIC_CHEF_ID, fiche.getChef_id());
		values.put(FIC_CHAN_ID, fiche.getChanter_id());
		values.put(FIC_EMP_ID, fiche.getEmploye_id());
		
		values.put(FIC_MAT_DEB, fiche.getMatin_deb());
		values.put(FIC_MAT_FIN, fiche.getMatin_fin());
		values.put(FIC_APR_DEB, fiche.getAprem_deb());
		values.put(FIC_APR_FIN, fiche.getAprem_fin());
		
		values.put(FIC_TRAJET, fiche.isTrajet());
		values.put(FIC_REPAS, fiche.isRepas());
		values.put(FIC_GD, fiche.isGd());

		return bdd.insert(TABLE_FICHE, null, values);
	}

	private Fiche cursorToFiche(Cursor c){
		if (c.getCount() == 0)
			return null;

		Fiche fiche = new Fiche();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		fiche.setJour(c.getString(1));
		fiche.setChef_id(c.getLong(2));
		fiche.setChanter_id(c.getLong(3));
		fiche.setEmploye_id(c.getLong(4));
		fiche.setMatin_deb(c.getString(5));
		fiche.setMatin_fin(c.getString(6));
		fiche.setAprem_deb(c.getString(7));
		fiche.setAprem_fin(c.getString(8));
		fiche.setTrajet(c.getInt(9)!=0);
		fiche.setRepas(c.getInt(10)!=0);
		fiche.setGd(c.getInt(11)!=0);

		return fiche;
	}

	public Vector<Fiche> getAll(){
		Vector<Fiche> vector = new Vector<Fiche>();
		Cursor cursor = this.bdd.query(TABLE_FICHE,
				this.allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Fiche fiche = cursorToFiche(cursor);
			if (fiche!=null)
				vector.add(fiche);
			cursor.moveToNext();
		}
		cursor.close();
		return vector;
	}
	
}
