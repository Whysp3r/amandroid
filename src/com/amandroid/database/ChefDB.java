package com.amandroid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amandroid.models.Chef;

public class ChefDB {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	
	private static final String TABLE_CHEF = "table_chef";
	private static final String CHEF_ID = "ID";
	private static final String CHEF_LOG = "Login";
	private static final String CHEF_PASS = "Pass";
	private static final String CHEF_IDSQL = "IDSQL";
	
	private String[] allColumns = {CHEF_ID, CHEF_LOG, CHEF_PASS, CHEF_IDSQL };
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public ChefDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}
	public int deleteAll(){
		return this.bdd.delete(TABLE_CHEF, null, null);
	}
 
	public long addChef(Chef chef){
		
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(CHEF_LOG, chef.getLogin());
		values.put(CHEF_PASS, chef.getPass());
		values.put(CHEF_IDSQL, chef.getId_mysql());
		
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_CHEF, null, values);
	}
	
	public Chef getChefByLoginAndPass(String log, String pass){
		//Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD 
		Cursor c = bdd.query(TABLE_CHEF, allColumns, 
						  CHEF_LOG + " = \"" + log +"\" AND "
						+ CHEF_PASS + " = \"" + pass +"\"", null, null, null, null);
		Chef chef = new Chef();
		c.moveToFirst();
		chef = cursorToChef(c);
		c.close();
		return chef ;
	}
	
	private Chef cursorToChef(Cursor c){
		if (c.getCount() == 0)
			return null;

		Chef chef = new Chef();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		chef.setId(c.getInt(0));
		chef.setLogin(c.getString(1));
		chef.setPass(c.getString(2));
		chef.setId_mysql(c.getString(3));

		//On retourne le chef
		return chef;
	}
}
