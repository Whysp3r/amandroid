package com.amandroid.database;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amandroid.models.Chantier;
import com.amandroid.models.Employe;
import com.amandroid.models.Interim;

public class EmployeDB {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	
	private static final String TABLE_EMPLOYE = "table_employe";
	private static final String EMP_ID = "ID";
	private static final String EMP_PRE = "Prenom";
	private static final String EMP_NOM = "Nom";
	private static final String EMP_IDSQL = "IDSQL";
	private static final String EMP_TEL = "Tel";
	private static final String EMP_ADD = "Adresse";
	private static final String EMP_VIL = "Ville";
	private static final String EMP_COM = "Commentaire";
	private static final String EMP_ID_AG = "IDAGE";
	
	private String[] allColumns = {EMP_ID,EMP_PRE,EMP_NOM,EMP_IDSQL,EMP_TEL,EMP_ADD,EMP_VIL,EMP_COM,EMP_ID_AG};
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public EmployeDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}
	public int deleteAll(){
		return this.bdd.delete(TABLE_EMPLOYE, null, null);
	}
 
	public long addEmploye(Employe employe){
		
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(EMP_PRE, employe.getPrenom());
		values.put(EMP_NOM, employe.getNom());
		values.put(EMP_IDSQL, employe.getId_mysql());
		values.put(EMP_TEL, employe.getTel());
		values.put(EMP_ADD, employe.getAdd());
		values.put(EMP_VIL, employe.getVille());
		values.put(EMP_COM, employe.getCommentaire());
		values.put(EMP_ID_AG, employe.getId_agence());
		
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_EMPLOYE, null, values);
	}
	
	public Employe getEmployeByNomAndPrenom(String nom, String prenom){
		//Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD 
		Cursor c = bdd.query(TABLE_EMPLOYE, allColumns, 
						  EMP_NOM + " = \"" + nom +"\" AND "
						+ EMP_PRE + " = \"" + prenom +"\"", null, null, null, null);
		Employe employe = new Employe();
		c.moveToFirst();
		employe = cursorToEmploye(c);
		c.close();
		return employe;
	}
	
	public Employe getEmployeByIdSql(String id){
		//Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD 
		Cursor c = bdd.query(TABLE_EMPLOYE, allColumns, EMP_IDSQL + " = \"" + id +"\"", null, null, null, null);
		Employe employe = new Employe();
		c.moveToFirst();
		employe = cursorToEmploye(c);
		c.close();
		return employe;
	}
	
	private Employe cursorToEmploye(Cursor c){
		if (c.getCount() == 0)
			return null;

		Employe employe = new Employe();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		employe.setId(c.getInt(0));
		employe.setPrenom(c.getString(1));
		employe.setNom(c.getString(2));
		employe.setId_mysql(c.getString(3));
		employe.setTel(c.getString(4));
		employe.setAdd(c.getString(5));
		employe.setVille(c.getString(6));
		employe.setCommentaire(c.getString(7));
		employe.setId_agence(c.getString(8));

		return employe;
	}
	
	public Vector<Employe> getAll(){
		Vector<Employe> vector = new Vector<Employe>();
		Cursor cursor = this.bdd.query(TABLE_EMPLOYE, 
				this.allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Employe employe = cursorToEmploye(cursor);
			if (employe!=null)
				vector.add(employe);
			cursor.moveToNext();
		}
		cursor.close();
		return vector;
	}
}
