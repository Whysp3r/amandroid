package com.amandroid.database;

import java.util.Vector;

import com.amandroid.models.Chantier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ChantierDB {
	 
	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	
	private static final String TABLE_CHANTIER = "table_chantier";
	
	private static final String COL_ID = "ID";
	private static final int NUM_COL_ID = 0;
	
	private static final String COL_NUM = "Num";
	private static final int NUM_COL_NUM = 1;
	
	private static final String COL_NOM = "Nom";
	private static final int NUM_COL_NOM = 2;
	
	private static final String COL_IDSQL = "IDSQL";
	private static final int NUM_COL_IDSQL = 3;
	
	private String[] allColumns = {COL_ID, COL_NUM, COL_NOM, COL_IDSQL };
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public ChantierDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}
	public int deleteAll(){
		return this.bdd.delete(TABLE_CHANTIER, null, null);
	}
 
	public long addChantier(Chantier chantier){
		
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(COL_NUM, chantier.getNum());
		values.put(COL_NOM, chantier.getNom());
		values.put(COL_IDSQL, chantier.getId_mysql());
		
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_CHANTIER, null, values);
	}
 
	public int updateChantier(int id, Chantier chantier){
		//La mise à jour d'un chantier dans la BDD fonctionne plus ou moins comme une insertion
		//il faut simple préciser quel chantier on doit mettre à jour grâce à l'ID
		ContentValues values = new ContentValues();
		values.put(COL_NUM, chantier.getNum());
		values.put(COL_NOM, chantier.getNom());
		values.put(COL_IDSQL, chantier.getId_mysql());
		return bdd.update(TABLE_CHANTIER, values, COL_ID + " = " +id, null);
	}
 
	public int removeChantierByID(int id){
		//Suppression d'un livre de la BDD grâce à l'ID
		return bdd.delete(TABLE_CHANTIER, COL_ID + " = " +id, null);
	}
	
	public Chantier getChantierByNom(String nom){
		//Récupère dans un Cursor les valeurs correspondant à un chantier contenu dans la BDD (ici on sélectionne le chantier grâce à son titre)
		Cursor c = bdd.query(TABLE_CHANTIER, new String[] {COL_ID, COL_NUM, COL_NOM}, COL_NOM + " LIKE \"" + nom +"\"", null, null, null, null);
		Chantier chantier = new Chantier();
		c.moveToFirst();
		chantier = cursorToChantier(c);
		c.close();
		return chantier ;
	}
 
	//Cette méthode permet de convertir un cursor en un Chantier
	private Chantier cursorToChantier(Cursor c){
		if (c.getCount() == 0)
			return null;
 

		Chantier chantier = new Chantier();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		chantier.setId(c.getInt(NUM_COL_ID));
		chantier.setNum(c.getString(NUM_COL_NUM));
		chantier.setNom(c.getString(NUM_COL_NOM));
		chantier.setId_mysql(c.getString(NUM_COL_IDSQL));

		//On retourne le chantier
		return chantier;
	}
	
	public Vector<Chantier> getAll(){
		Vector<Chantier> vector = new Vector<Chantier>();
		Cursor cursor = this.bdd.query(TABLE_CHANTIER, 
				this.allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Chantier chantier = cursorToChantier(cursor);
			if (chantier!=null)
				vector.add(chantier);
			cursor.moveToNext();
		}
		cursor.close();
		return vector;
	}
}