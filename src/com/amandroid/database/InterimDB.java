package com.amandroid.database;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amandroid.models.Chantier;
import com.amandroid.models.Employe;
import com.amandroid.models.Interim;

public class InterimDB {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	
	private static final String TABLE_INTERIM = "table_interim";
	private static final String INT_ID = "ID";
	private static final String INT_PRE = "Prenom";
	private static final String INT_NOM = "Nom";
	private static final String INT_IDSQL = "IDSQL";
	private static final String INT_TEL = "Tel";
	private static final String INT_ADD = "Adresse";
	private static final String INT_VIL = "Ville";
	private static final String INT_COM = "Commentaire";
	private static final String INT_ID_AG = "IDAGE";
	
	private String[] allColumns = {INT_ID,INT_PRE,INT_NOM,INT_IDSQL,INT_TEL,INT_ADD,INT_VIL,INT_COM,INT_ID_AG};
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public InterimDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}
	public int deleteAll(){
		return this.bdd.delete(TABLE_INTERIM, null, null);
	}
 
	public long addInterim(Interim interim){
		
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(INT_PRE, interim.getPrenom());
		values.put(INT_NOM, interim.getNom());
		values.put(INT_IDSQL, interim.getId_mysql());
		values.put(INT_TEL, interim.getTel());
		values.put(INT_ADD, interim.getAdd());
		values.put(INT_VIL, interim.getVille());
		values.put(INT_COM, interim.getCommentaire());
		values.put(INT_ID_AG, interim.getId_agence());
		
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_INTERIM, null, values);
	}
	
	public Interim getInterimByNomAndPrenom(String nom, String prenom){
		//Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD 
		Cursor c = bdd.query(TABLE_INTERIM, allColumns, 
						  INT_NOM + " = \"" + nom +"\" AND "
						+ INT_PRE + " = \"" + prenom +"\"", null, null, null, null);
		Interim interim = new Interim();
		c.moveToFirst();
		interim = cursorToInterim(c);
		c.close();
		return interim;
	}
	
	public Interim getInterimByIdSql(String id){
		//Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD 
		Cursor c = bdd.query(TABLE_INTERIM, allColumns, INT_IDSQL + " = \"" + id +"\"", null, null, null, null);
		Interim interim = new Interim();
		c.moveToFirst();
		interim = cursorToInterim(c);
		c.close();
		return interim;
	}
	
	private Interim cursorToInterim(Cursor c){
		if (c.getCount() == 0)
			return null;

		Interim interim = new Interim();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		interim.setId(c.getInt(0));
		interim.setPrenom(c.getString(1));
		interim.setNom(c.getString(2));
		interim.setId_mysql(c.getString(3));
		interim.setTel(c.getString(4));
		interim.setAdd(c.getString(5));
		interim.setVille(c.getString(6));
		interim.setCommentaire(c.getString(7));
		interim.setId_agence(c.getString(8));

		//On retourne le chef
		return interim;
	}
	
	public Vector<Interim> getAll(){
		Vector<Interim> vector = new Vector<Interim>();
		Cursor cursor = this.bdd.query(TABLE_INTERIM, 
				this.allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			Interim interim = cursorToInterim(cursor);
			if (interim!=null)
				vector.add(interim);
			cursor.moveToNext();
		}
		cursor.close();
		return vector;
	}
}
