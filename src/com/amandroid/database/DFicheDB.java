package com.amandroid.database;

import java.util.Vector;

import com.amandroid.models.Chantier;
import com.amandroid.models.DFiche;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DFicheDB {
	 
	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "am.db";
	private static final String TABLE_D_FICHES = "table_derniere_fiche";
	
	private static final String DFI_ID = "ID";
	private static final String DFI_JOUR = "Jour";
	private static final String DFI_IDCHA = "Id_chant";
	private static final String DFI_IDCHE = "Id_chef";
	
	private String[] allColumns = {DFI_ID, DFI_JOUR, DFI_IDCHA, DFI_IDCHE };
	
	private SQLiteDatabase bdd;
 
	private MaBaseSQLite maBaseSQLite;
 
	public DFicheDB(Context context){
		//On créer la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}
	public int deleteAll(){
		return this.bdd.delete(TABLE_D_FICHES, null, null);
	}
 
	public long addDFiche(DFiche dfiche){
		
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
		values.put(DFI_JOUR, dfiche.getJour());
		values.put(DFI_IDCHA, dfiche.getId_chant());
		values.put(DFI_IDCHE, dfiche.getId_chef());
		
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_D_FICHES, null, values);
	}
 
	public int updateDFiche(int id, DFiche dfiche){
		//La mise à jour d'un chantier dans la BDD fonctionne plus ou moins comme une insertion
		//il faut simple préciser quel chantier on doit mettre à jour grâce à l'ID
		ContentValues values = new ContentValues();
		values.put(DFI_JOUR, dfiche.getJour());
		values.put(DFI_IDCHA, dfiche.getId_chant());
		values.put(DFI_IDCHE, dfiche.getId_chef());
		return bdd.update(TABLE_D_FICHES, values, DFI_ID + " = " +id, null);
	}
 
	public int removeDFicheByID(int id){
		//Suppression d'un livre de la BDD grâce à l'ID
		return bdd.delete(TABLE_D_FICHES, DFI_ID + " = " +id, null);
	}
	

	//Cette méthode permet de convertir un cursor en un Chantier
	private DFiche cursorToDFiche(Cursor c){
		if (c.getCount() == 0)
			return null;
 

		DFiche dfiche = new DFiche();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		dfiche.setId(c.getInt(0));
		dfiche.setJour(c.getString(1));
		dfiche.setId_chant(c.getString(2));
		dfiche.setId_chef(c.getString(3));

		//On retourne le chantier
		return dfiche;
	}
	
	public Vector<DFiche> getAll(){
		Vector<DFiche> vector = new Vector<DFiche>();
		Cursor cursor = this.bdd.query(TABLE_D_FICHES, 
				this.allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			DFiche dfiche = cursorToDFiche(cursor);
			if (dfiche!=null)
				vector.add(dfiche);
			cursor.moveToNext();
		}
		cursor.close();
		return vector;
	}
}