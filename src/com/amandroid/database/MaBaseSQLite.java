package com.amandroid.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseSQLite extends SQLiteOpenHelper {
	
	//On créer la table CHEF ------------------------------------------------------------------>
	private static final String TABLE_CHEF = "table_chef";
	private static final String CHEF_ID = "ID";
	private static final String CHEF_LOG = "Login";
	private static final String CHEF_PASS = "Pass";
	private static final String CHEF_IDSQL = "IDSQL";
	
	private static final String CREATE_CHEF = "CREATE TABLE " + TABLE_CHEF + " ("
			+ CHEF_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CHEF_LOG + " TEXT NOT NULL, "
			+ CHEF_PASS + " TEXT NOT NULL,"
			+ CHEF_IDSQL+ " TEXT NOT NULL );";
	
	
	//On créer la table CHANTIER --------------------------------------------------------------->
	private static final String TABLE_CHANTIER = "table_chantier";
	private static final String COL_ID = "ID";
	private static final String COL_NUM = "Num";
	private static final String COL_NOM = "Nom";
	private static final String COL_IDSQL = "IDSQL";
	
	private static final String CREATE_CHANTIER = "CREATE TABLE " + TABLE_CHANTIER + " ("
	+ COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NUM + " TEXT NOT NULL, "
	+ COL_NOM + " TEXT NOT NULL,"
	+ COL_IDSQL+ " TEXT NOT NULL );";
	
	
	//On créer la table D_FICHES (Dernières fiches) -------------------------------------------->
	private static final String TABLE_D_FICHES = "table_derniere_fiche";
	private static final String DFI_ID = "ID";
	private static final String DFI_JOUR = "Jour";
	private static final String DFI_IDCHA = "Id_chant";
	private static final String DFI_IDCHE = "Id_chef";
	
	private static final String CREATE_D_FICHE = "CREATE TABLE " + TABLE_D_FICHES + " ("
	+ DFI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + DFI_JOUR + " TEXT NOT NULL, "
	+ DFI_IDCHA + " TEXT NOT NULL,"
	+ DFI_IDCHE+ " TEXT NOT NULL );";
	
	
	//On créer la table EMPLOYE ---------------------------------------------------------------->
	private static final String TABLE_EMPLOYE = "table_employe";
	private static final String EMP_ID = "ID";
	private static final String EMP_PRE = "Prenom";
	private static final String EMP_NOM = "Nom";
	private static final String EMP_IDSQL = "IDSQL";
	private static final String EMP_TEL = "Tel";
	private static final String EMP_ADD = "Adresse";
	private static final String EMP_VIL = "Ville";
	private static final String EMP_COM = "Commentaire";
	private static final String EMP_ID_AG = "IDAGE";

	private static final String CREATE_EMPLOYE = "CREATE TABLE " + TABLE_EMPLOYE + " ("
	+ EMP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
	+ EMP_PRE + " TEXT NOT NULL, "
	+ EMP_NOM + " TEXT NOT NULL,"
	+ EMP_IDSQL+ " TEXT NOT NULL,"	
	+ EMP_TEL+ " TEXT ,"
	+ EMP_ADD+ " TEXT ,"
	+ EMP_VIL+ " TEXT ,"
	+ EMP_COM+ " TEXT ,"
	+ EMP_ID_AG + " TEXT );";
	
	//On créer la table INTERIM ---------------------------------------------------------------->
	private static final String TABLE_INTERIM = "table_interim";
	private static final String INT_ID = "ID";
	private static final String INT_PRE = "Prenom";
	private static final String INT_NOM = "Nom";
	private static final String INT_IDSQL = "IDSQL";
	private static final String INT_TEL = "Tel";
	private static final String INT_ADD = "Adresse";
	private static final String INT_VIL = "Ville";
	private static final String INT_COM = "Commentaire";
	private static final String INT_ID_AG = "IDAGE";

	private static final String CREATE_INTERIM = "CREATE TABLE " + TABLE_INTERIM + " ("
	+ INT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
	+ INT_PRE + " TEXT NOT NULL, "
	+ INT_NOM + " TEXT NOT NULL,"
	+ INT_IDSQL+ " TEXT NOT NULL,"	
	+ INT_TEL+ " TEXT ,"
	+ INT_ADD+ " TEXT ,"
	+ INT_VIL+ " TEXT ,"
	+ INT_COM+ " TEXT ,"
	+ INT_ID_AG+ " TEXT );";

	//On créer la table CONTACT ---------------------------------------------------------------->
	private static final String TABLE_CONTACT = "table_contact";
	private static final String CON_ID = "ID";
	private static final String CON_PRE = "Prenom";
	private static final String CON_NOM = "Nom";
	private static final String CON_IDSQL = "IDSQL";
	private static final String CON_TEL = "Tel";
	private static final String CON_ADD = "Adresse";
	private static final String CON_VIL = "Ville";
	private static final String CON_COM = "Commentaire";
	private static final String CON_STA = "Statut";
	private static final String CON_EMA = "Email";

	private static final String CREATE_CONTACT = "CREATE TABLE " + TABLE_CONTACT + " ("
			+ CON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ CON_PRE + " TEXT NOT NULL, "
			+ CON_NOM + " TEXT NOT NULL,"
			+ CON_IDSQL+ " TEXT NOT NULL,"
			+ CON_TEL+ " TEXT ,"
			+ CON_ADD+ " TEXT ,"
			+ CON_VIL+ " TEXT ,"
			+ CON_COM+ " TEXT ,"
			+ CON_STA+ " TEXT ,"
			+ CON_EMA+ " TEXT );";
	
	//On créer la table FICHE ---------------------------------------------------------------->
	private static final String TABLE_FICHE = "table_fiche";
	private static final String FIC_ID = "ID";
	private static final String FIC_JOUR = "Jour";
	private static final String FIC_CHEF_ID = "Chef_id";
	private static final String FIC_CHAN_ID = "Chantier_id";
	private static final String FIC_EMP_ID = "Employe_id";
	private static final String FIC_MAT_DEB = "Matin_debut";
	private static final String FIC_MAT_FIN = "Matin_fin";
	private static final String FIC_APR_DEB = "Aprem_debut";
	private static final String FIC_APR_FIN = "Aprem_fin";
	private static final String FIC_TRAJET = "Trajet";
	private static final String FIC_REPAS = "Repas";
	private static final String FIC_GD = "Grand_Deplacement";


	private static final String CREATE_FICHE = "CREATE TABLE " + TABLE_FICHE + " ("
	+ FIC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
	+ FIC_JOUR + " TEXT NOT NULL, "
	+ FIC_CHEF_ID + " TEXT NOT NULL,"
	+ FIC_CHAN_ID+ " TEXT NOT NULL,"	
	+ FIC_EMP_ID+ " TEXT ,"
	+ FIC_MAT_DEB+ " TEXT ,"
	+ FIC_MAT_FIN+ " TEXT ,"
	+ FIC_APR_DEB+ " TEXT ,"
	+ FIC_APR_FIN+ " TEXT ,"
	+ FIC_TRAJET+ " TEXT ,"
	+ FIC_REPAS+ " TEXT ,"
	+ FIC_GD+ " TEXT );";
	
	//------------------------------------------------------------------------------------------>

	public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CHANTIER);
		db.execSQL(CREATE_CHEF);
		db.execSQL(CREATE_EMPLOYE);
		db.execSQL(CREATE_INTERIM);
		db.execSQL(CREATE_CONTACT);
		db.execSQL(CREATE_D_FICHE);
		db.execSQL(CREATE_FICHE);	
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE " + TABLE_CHANTIER + ";");
		db.execSQL("DROP TABLE " + TABLE_CHEF + ";");
		db.execSQL("DROP TABLE " + TABLE_EMPLOYE + ";");
		db.execSQL("DROP TABLE " + TABLE_INTERIM + ";");
		db.execSQL("DROP TABLE " + TABLE_CONTACT + ";");
		db.execSQL("DROP TABLE " + TABLE_D_FICHES + ";");
		db.execSQL("DROP TABLE " + TABLE_FICHE + ";");
		onCreate(db);
	}
 
}