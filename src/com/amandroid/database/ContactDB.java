package com.amandroid.database;

import java.util.Vector;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.amandroid.models.Contact;
import com.amandroid.models.Interim;

public class ContactDB {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "am.db";

    private static final String TABLE_CONTACT = "table_contact";
    private static final String CON_ID = "ID";
    private static final String CON_PRE = "Prenom";
    private static final String CON_NOM = "Nom";
    private static final String CON_IDSQL = "IDSQL";
    private static final String CON_TEL = "Tel";
    private static final String CON_ADD = "Adresse";
    private static final String CON_VIL = "Ville";
    private static final String CON_COM = "Commentaire";
    private static final String CON_STA = "Statut";
    private static final String CON_EMA = "Email";

    private String[] allColumns = {CON_ID,CON_PRE,CON_NOM,CON_IDSQL,CON_TEL,CON_ADD,CON_VIL,CON_COM,CON_STA,CON_EMA};

    private SQLiteDatabase bdd;

    private MaBaseSQLite maBaseSQLite;

    public ContactDB(Context context){
        //On créer la BDD et sa table
        maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }
    public int deleteAll(){
        return this.bdd.delete(TABLE_CONTACT, null, null);
    }

    public long addContact(Contact contact){

        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();

        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(CON_PRE, contact.getPrenom());
        values.put(CON_NOM, contact.getNom());
        values.put(CON_IDSQL, contact.getId_mysql());
        values.put(CON_TEL, contact.getTel());
        values.put(CON_ADD, contact.getAdd());
        values.put(CON_VIL, contact.getVille());
        values.put(CON_COM, contact.getCommentaire());
        values.put(CON_STA, contact.getStatut());
        values.put(CON_EMA,contact.getEmail());

        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_CONTACT, null, values);
    }

    public Contact getContactByIdSql(String id){
        //Récupère dans un Cursor les valeurs correspondant à un chef contenu dans la BDD
        Cursor c = bdd.query(TABLE_CONTACT, allColumns, CON_IDSQL + " = \"" + id +"\"", null, null, null, null);
        Contact contact = new Contact();
        c.moveToFirst();
        contact = cursorToContact(c);
        c.close();
        return contact;
    }

    private Contact cursorToContact(Cursor c){
        if (c.getCount() == 0)
            return null;

        Contact contact = new Contact();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        contact.setId(c.getInt(0));
        contact.setPrenom(c.getString(1));
        contact.setNom(c.getString(2));
        contact.setId_mysql(c.getString(3));
        contact.setTel(c.getString(4));
        contact.setAdd(c.getString(5));
        contact.setVille(c.getString(6));
        contact.setCommentaire(c.getString(7));
        contact.setStatut(c.getString(8));
        contact.setEmail(c.getString(9));

        //On retourne le chef
        return contact;
    }

    public Vector<Contact> getAll(){
        Vector<Contact> vector = new Vector<Contact>();
        Cursor cursor = this.bdd.query(TABLE_CONTACT,
                this.allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Contact contact = cursorToContact(cursor);
            if (contact!=null)
                vector.add(contact);
            cursor.moveToNext();
        }
        cursor.close();
        return vector;
    }
}
