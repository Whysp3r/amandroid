package com.amandroid.adapter;

import java.util.ArrayList;

import com.amandroid.R;
import com.amandroid.models.DetailsFicheItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsFicheAdapter extends ArrayAdapter<DetailsFicheItem> {
     
    private final Context context;
    private final ArrayList<DetailsFicheItem> itemsArrayList;

    public DetailsFicheAdapter(Context context, ArrayList<DetailsFicheItem> itemsArrayList) {

        super(context, R.layout.rowfichedetails, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        
        // 1. creer un inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. récupérer la vue de la ligne de la fiche
        View rowView = inflater.inflate( R.layout.rowfichedetails, parent, false);

        // 3. récupérer les différents éléments à remplir
        TextView nomPrenomView = (TextView) rowView.findViewById(R.id.nomprenom);
        TextView horairesView = (TextView) rowView.findViewById(R.id.horaires);
        ImageView deplacementView = (ImageView) rowView.findViewById(R.id.deplacement);
        ImageView repasView = (ImageView) rowView.findViewById(R.id.repas);
        ImageView trajetView = (ImageView) rowView.findViewById(R.id.trajet);
       
        // 4. modifier les différent élément
        nomPrenomView.setText(itemsArrayList.get(position).getNomPrenom());
        horairesView.setText(itemsArrayList.get(position).getMatin_debut()+" - "+itemsArrayList.get(position).getMatin_fin()+"  "+itemsArrayList.get(position).getAprem_debut()+" - "+itemsArrayList.get(position).getAprem_fin());
       
        if(!itemsArrayList.get(position).getGranddeplacement()){
            deplacementView.setVisibility(View.INVISIBLE);
        }
        
        if(!itemsArrayList.get(position).getRepas()){
            repasView.setVisibility(View.INVISIBLE);
        }
        
        if(!itemsArrayList.get(position).getTrajet()){
            trajetView.setVisibility(View.INVISIBLE);
        }
        
        // 5. retourner la ligne
        return rowView;
    }
}
