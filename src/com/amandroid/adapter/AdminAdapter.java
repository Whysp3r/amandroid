package com.amandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.amandroid.R;
import com.amandroid.models.Worker;

import java.util.ArrayList;

/**
* Created by Whysper on 28/11/2014.
*/
public class AdminAdapter extends ArrayAdapter<Worker> {
    private final Context context;
    private final ArrayList<Worker> values;
   //Resources resc = getContext().getResources();

    public AdminAdapter(Context context, ArrayList<Worker> values) {
        super(context, R.layout.rowadminlistview, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public Worker getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.rowadminlistview, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.nom_prenom);
        textView.setText(values.get(position).getName());
        return rowView;
    }

}
