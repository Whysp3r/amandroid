package com.amandroid.serializers;

import java.lang.reflect.Type;

import com.amandroid.models.Fiche;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


public class FicheSerializer implements JsonSerializer<Fiche>, JsonDeserializer<Fiche>  {

	@Override
	public JsonElement serialize(Fiche fiche, Type arg1,
			JsonSerializationContext arg2) {
		JsonObject result = new JsonObject();
		
        result.add("chantiers_has_chefs_chantiers_id", new JsonPrimitive(fiche.getChanter_id()));
        result.add("chantiers_has_chefs_chefs_id", new JsonPrimitive(fiche.getChef_id()));
        result.add("employe_id", new JsonPrimitive(fiche.getEmploye_id()));
        result.add("jour", new JsonPrimitive(fiche.getJour()));
        result.add("granddeplacement", new JsonPrimitive(fiche.isGd()));
        result.add("matin_debut", new JsonPrimitive(fiche.getMatin_deb()));
        result.add("matin_fin", new JsonPrimitive(fiche.getMatin_fin()));
        result.add("aprem_debut", new JsonPrimitive(fiche.getAprem_deb()));
        result.add("aprem_fin", new JsonPrimitive(fiche.getAprem_fin()));
        result.add("trajet", new JsonPrimitive(fiche.isTrajet()));
        result.add("repas", new JsonPrimitive(fiche.isRepas()));
           
        return result;
	}

	@Override
	public Fiche deserialize(JsonElement json, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {
		JsonObject jsonObject = json.getAsJsonObject();
		Fiche Fiche = new Fiche(jsonObject.get("jour").getAsString(), 
							jsonObject.get("chantiers_has_chefs_chantiers_id").getAsLong(),
							jsonObject.get("chantiers_has_chefs_chefs_id").getAsLong(),
							jsonObject.get("employe_id").getAsLong(),
							jsonObject.get("matin_debut").getAsString(),
							jsonObject.get("matin_fin").getAsString(),
							jsonObject.get("aprem_debut").getAsString(),
							jsonObject.get("aprem_fin").getAsString(),
							jsonObject.get("trajet").getAsBoolean(),
							jsonObject.get("repas").getAsBoolean(),
							jsonObject.get("granddeplacement").getAsBoolean()
							);
		
		return Fiche;
	}

}

