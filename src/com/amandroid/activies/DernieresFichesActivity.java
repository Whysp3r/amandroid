package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.ChantierDB;
import com.amandroid.database.DFicheDB;
import com.amandroid.models.Chantier;
import com.amandroid.models.DFiche;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class DernieresFichesActivity extends Activity {
	
	 ListView listView ;
	 JSONArray listeDernieresFiches;
	 String[] fiches;
	 long idChef;
	 long idChant;
	 int loadModel;
	 private Vector<DFiche> dficheVector;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dernieresfiches);
        
        Intent intent = getIntent();
        
	    //On récupère l'id du chantier et du chef séléctionné
		String idChantier = intent.getStringExtra("idChantier");
		idChef = intent.getLongExtra("idChef", 0);
		idChant = Long.parseLong(idChantier);
		
		//On recupère la listView
		listView = (ListView) findViewById(R.id.listDernieresFiches);
		
		//Création d'une instance de ma classe ChantierDB
        DFicheDB dficheBDD = new DFicheDB(this);
        
        //On recupère la liste de chantiers de la dernière synchro
        dficheBDD.open();
        dficheVector = dficheBDD.getAll();
        dficheBDD.close();
		
		//On récupère la liste des dernières fiches
		try {
			listeDernieresFiches = getDernieresFiches(idChantier);
			
			if(listeDernieresFiches!=null){
			int nbFiches = listeDernieresFiches.length();
			fiches = new String[nbFiches];
			
		    //On ouvre la base de données pour écrire dedans
			dficheBDD.open();
			dficheBDD.deleteAll();
			
			//On peuple la liste des chantiers à partir du json object
	        for(int i=0; i < nbFiches; i++){       
	     
	        	JSONObject chantierObject = listeDernieresFiches.getJSONObject(i);
	            String jourFiche =  chantierObject.getJSONObject("Fiche").getString("jour");	  
	            String idChantierFiche =  chantierObject.getJSONObject("Fiche").getString("chantiers_has_chefs_chantiers_id");
	            String idChefFiche =  chantierObject.getJSONObject("Fiche").getString("chantiers_has_chefs_chefs_id"); 
	           
	           
	            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	            DateFormat formatterDisplay = new SimpleDateFormat("EEEE dd MMMM yyyy");
	            
	            java.util.Date date;
				try {
					date = formatter.parse(jourFiche);
					jourFiche = DateFormat.getDateInstance(DateFormat.FULL,Locale.FRANCE).format(date);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		
	            fiches[i] = jourFiche;
	            
	            
	            //Création d'un chantier
	            DFiche dfiche = new DFiche(jourFiche,idChantierFiche,idChefFiche );
	            dficheBDD.addDFiche(dfiche);    
	        }	
	        //On ferme la base de donnée
	        dficheBDD.close();				
			}
			else{
				System.out.println("Aucun accès réseau");
			}
		}
		catch (JSONException e) {
	
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//On créer l'adapter et on lui assigne la list view	 
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			this,
			android.R.layout.simple_list_item_1, 
			android.R.id.text1, 
			fiches
		);		  
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new ListView.OnItemClickListener() {  		
	           @Override
	           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {       
	           		
	           		try {
	           			Intent viewFicheDetails = new Intent (view.getContext(),DetailsFicheActivity.class);
						JSONObject ficheObject = listeDernieresFiches.getJSONObject(position);
						viewFicheDetails.putExtra("fiche", ficheObject.toString());
						startActivity(viewFicheDetails);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	           		
	           		
	        		
	        	    
	           		
	           }    
	      });
    }
    
    public JSONArray getDernieresFiches (String idChantier) throws JSONException{
    	String json =null;
		JSONArray listeDerniersFichesArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String param =idChantier+'/'+idChef;
			String urlString = "http://www.alpex-epdm.fr/AM/api/getLastFichesByChantierAndChef/"+param;
			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			listeDerniersFichesArray = new JSONArray(json);
            return listeDerniersFichesArray;	
		}
		else{
			//TODO
			//Récupèrer la liste des dernière fiche en local (SQlite)
			if(dficheVector!=null){
				fiches = new String[dficheVector.size()];
				for(int j =0; j<dficheVector.size(); j++){
					fiches[j] =  dficheVector.get(j).getJour();				
				}
			}
			listeDerniersFichesArray = null;
			return listeDerniersFichesArray;	
		}
    }
    
    public void Ajouter(View view){ 
		
		if(listeDernieresFiches.length()>0){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setMessage("Charger le dernier modèle ?");
	        builder.setCancelable(true);
	        builder.setPositiveButton("Oui",
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	            	loadModel = 1;
	            	Intent i = createIntent();
	        		startActivity(i);  
	                dialog.cancel();
	            }
	        });
	        builder.setNegativeButton("Non",
	                new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	            	loadModel = 0;
	            	Intent i = createIntent();
	        		startActivity(i);  
	                dialog.cancel();
	            }
	        });
	        
	        AlertDialog alert = builder.create();
	        alert.show();
		}else{
			loadModel = 0;
			Intent i = createIntent();
    		startActivity(i); 
		}
		
    }
    
    public Intent createIntent(){
    	Intent i = new Intent(this,IntervenantsActivity.class);
    	
		i.putExtra("idChef",idChef);
		i.putExtra("idChant", idChant);
		i.putExtra("loadModel", loadModel);
		if(loadModel==1){
			JSONObject ficheObject;
			try {
				ficheObject = listeDernieresFiches.getJSONObject(0);
				i.putExtra("derniereFiche", ficheObject.toString());
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return i;
		
    }
}