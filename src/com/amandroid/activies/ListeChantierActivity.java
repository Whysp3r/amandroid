package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;
import java.util.concurrent.ExecutionException;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.ChantierDB;
import com.amandroid.database.MaBaseSQLite;
import com.amandroid.models.Chantier;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class ListeChantierActivity extends Activity{
	
	 ListView listView ;
	 Menu menu;
	 
	 long idChef;
	 String idChantier;
	 String[] chantiers ;
	 String[] listeIdChantiers;
	 JSONArray chantiersArray = new JSONArray();
	 private Vector<Chantier> chantierVector;
     
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_listechantier);
	    
	    Intent intent = getIntent();
	    
	    //On récupère l'id de l'empolyé connecté (chef) pour afficher les chantiers qui lui sont attribués
	    idChef = intent.getLongExtra("idEmploye", 0);
					
		//On recupère la listView
		listView = (ListView) findViewById(R.id.listChantierByChef);
		
		//Création d'une instance de ma classe ChantierDB
        ChantierDB chantierBdd = new ChantierDB(this);
    
        //On recupère la liste de chantiers de la dernière synchro
        chantierBdd.open();
        chantierVector = chantierBdd.getAll();
        chantierBdd.close();
	
		//On récupere les chantiers sous forme d'un tableau d'objets JSON
		try {
			chantiersArray = getChantierByChef(idChef);			
			if(chantiersArray!=null){	
				
		        int nbChantiers = chantiersArray.length();
		        chantiers = new String[nbChantiers];
		        listeIdChantiers = new String[nbChantiers];
		        
		        //On ouvre la base de données pour écrire dedans
	            chantierBdd.open();
	            chantierBdd.deleteAll();
		        
			        //On peuple la liste des chantiers à partir du json object
			        for(int i=0; i < nbChantiers; i++){       
			     
			        	JSONObject chantierObject = chantiersArray.getJSONObject(i);
			        	
			            String nomChantier =  chantierObject.getJSONObject("Chantier").getString("nom");	  
			            String numChantier =  chantierObject.getJSONObject("Chantier").getString("num");
			            String idChantier =  chantierObject.getJSONObject("Chantier").getString("id");
			            
			            //Création d'un chantier
			            Chantier chantier = new Chantier(numChantier,nomChantier,idChantier );
			            chantierBdd.addChantier(chantier);
		 	          
			            chantiers[i] = numChantier+" - "+nomChantier;
			            listeIdChantiers[i] = idChantier;
			        }
	            //On ferme la base de donnée
	            chantierBdd.close();
				}
			else{
				System.out.println("Aucun accès réseau");
			}
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
		}
		
		//On créer l'adapter et on lui assigne la list view	 
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				 android.R.layout.simple_list_item_1, android.R.id.text1, chantiers);
	  
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new ListView.OnItemClickListener() {  		
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {       
           		idChantier = listeIdChantiers[position];
           		System.out.println("idChantier Selected: "+idChantier);
           		
           		//On passe l'id du chantier ˆ l'intent et on lance la nouvelle activité.
        		Intent goToDernieresFiches = new Intent (view.getContext(),DernieresFichesActivity.class);
        		goToDernieresFiches.putExtra("idChantier",idChantier);
        		goToDernieresFiches.putExtra("idChef",idChef);
        	    startActivity(goToDernieresFiches);
           }    
      });
	}
	
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	
	public JSONArray getChantierByChef(long id) throws JSONException{
		
		String json =null;
		JSONArray listeChantierArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String param = Long.toString(id);
			String urlString = "http://www.alpex-epdm.fr/AM/api/getChantiersByChef/"+param;
			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
            listeChantierArray = new JSONArray(json);          
        return listeChantierArray;	
		} 
		else{
			//Récupèrer la liste des chantiers en local (SQlite)
			if(chantierVector!=null){
				
				chantiers = new String[chantierVector.size()];
				listeIdChantiers = new String[chantierVector.size()];
				
				for(int j =0; j<chantierVector.size(); j++){
					chantiers[j] =  chantierVector.get(j).getNum()+" - "+chantierVector.get(j).getNom();	
					listeIdChantiers[j] = chantierVector.get(j).getId_mysql();				
				}
			}
			listeChantierArray = null;
		return listeChantierArray;	
		}
		
	}

}
