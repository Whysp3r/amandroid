package com.amandroid.activies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.amandroid.R;

public class AdministrationActivity extends Activity {
	
	  Menu menu;
	  ListView listView ;
	  String[] categories = new String[3];

	  protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_administration);
	        
	        categories[0] = "Employés";
	        categories[1] = "Intérimaires";
	        categories[2] = "Administration";		

			listView = (ListView) findViewById(R.id.listeCategorie);
			
			//On créer l'adapter et on lui assigne la list view	 
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					 android.R.layout.simple_list_item_1, android.R.id.text1, categories);
		  
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(new ListView.OnItemClickListener() {  		
	           @Override
	           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {       	           		
	        		switch (position) {	
	    			case 0:
	    				Intent goToEmployes = new Intent(view.getContext(),AdminEmployeActivity.class);
	    				startActivity(goToEmployes);
	    				break;
	    			case 1:
	    				Intent goToInterims = new Intent(view.getContext(),AdminInterimActivity.class);
	    				startActivity(goToInterims);
	    				break;
	    			case 2:
	    				Intent goToContact = new Intent(view.getContext(),AdminContactActivity.class);
	    				startActivity(goToContact);
	    				break;
	    			default:
	    				break;
	    			}
	          }    
	      });
	    }
	  
	  @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}

}
