package com.amandroid.activies;

import java.util.ArrayList;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.database.DFicheDB;
import com.amandroid.database.FicheDB;
import com.amandroid.models.DFiche;
import com.amandroid.models.DetailsFicheItem;
import com.amandroid.models.Fiche;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.view.View.OnClickListener;

public class IntervenantsActivity extends Activity implements NumberPicker.OnValueChangeListener
{
	Menu menu;
	long idChef;
	long idChant;
	int loadModel;
	String derniereFiche;
	int nbParticipant;
	
    private int nbIntervenant;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervenants);
        nbIntervenant = 0;
        
        Intent intent = getIntent();
	    //On récupère l'id du chantier et du chef séléctionné
		idChef = intent.getLongExtra("idChef", 0); 
		idChant = intent.getLongExtra("idChant", 0); 
		loadModel = intent.getIntExtra("loadModel", 0);
		nbParticipant = 0;
		
		if(loadModel==1){
			derniereFiche = intent.getStringExtra("derniereFiche");
			nbParticipant =  generateData();
		}
		
		show();
    }
    
    @Override
   	public boolean onCreateOptionsMenu(Menu menu) {
   		// Inflate the menu; this adds items to the action bar if it is present.
   		super.onCreateOptionsMenu(menu);
   		this.menu = menu;
   		getMenuInflater().inflate(R.menu.main, menu);
   		return true;
   	}
    @Override
   	public boolean onOptionsItemSelected(MenuItem item) {
   		switch (item.getItemId()) {	
   			case R.id.action_administration:
   				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
   				startActivity(goToAdministration);
   				break;
   			case R.id.action_fiche:
   				Intent goToFiche = new Intent(this,LoginActivity.class);
   				startActivity(goToFiche);
   				break;
   			case R.id.action_settings:
   				Intent goToSettings = new Intent(this,SettingsActivity.class);
   				startActivity(goToSettings);
   				break;
   			default:
   				break;
   			}
   		return super.onOptionsItemSelected(item);		
   	}

    public void show(){
           
         Button valider = (Button) findViewById(R.id.validerButton);
         final NumberPicker np = (NumberPicker) findViewById(R.id.numberPicker1);
         np.setMaxValue(100);
         np.setMinValue(1);
         np.setWrapSelectorWheel(false);
         np.setOnValueChangedListener(this);
        
         if(nbParticipant!=0){
        	 
        	 np.setValue(nbParticipant);
         }
         
         valider.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
        	nbIntervenant=np.getValue();
            Intent goToSelectionEmploye = new Intent (v.getContext(),SelectionEmployeActivity.class);
            goToSelectionEmploye.putExtra("nbIntervenant",nbIntervenant);
            goToSelectionEmploye.putExtra("idChef",idChef);
            goToSelectionEmploye.putExtra("idChant",idChant);
            goToSelectionEmploye.putExtra("derniereFiche", derniereFiche);
            goToSelectionEmploye.putExtra("loadModel", loadModel);
            
    	    startActivity(goToSelectionEmploye); 
           }    
          }); 
    }

	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		// TODO Auto-generated method stub		
	}
	
	
	private int generateData(){
		
        try {
        	
			JSONObject jsonFiche = new JSONObject(derniereFiche);
			JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
			
			return details.length();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return 0;
    }
}