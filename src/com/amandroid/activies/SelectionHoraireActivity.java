package com.amandroid.activies;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.models.Fiche;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.serializables.FicheSerializable;
import com.amandroid.customs.CustomTimePickerDialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class SelectionHoraireActivity extends Activity  {
	
	ArrayList<Worker> listWorker = new ArrayList<Worker>();
	ArrayList<Fiche> listeFiches = new ArrayList<Fiche>();
	Fiche ficheEnCour;
	Menu menu;
	TextView tv;
	int incredEmploy =0;
	int hour,min;
	static final int TIME_DIALOG_ID=0;
	int idSelected;
	CustomTimePickerDialog timePickerDialog;
	
	//Variable pour les fiches à envoyer
	String dateOfDay;
	long idChef;
	long idChant;
	
	//Ids des textView
	static final int DEB_MAT_ID= R.id.debMatin;
	static final int FIN_MAT_ID= R.id.finMatin;
	static final int DEB_APR_ID= R.id.debAprem;
	static final int FIN_APR_ID= R.id.finAprem;
	
	//variable pour recopier l'heure automatiquement
	 String h1="";
	 String h2="";
	 String h3="";
	 String h4="";
	
	 int loadModel;
	 String derniereFiche; 
	 
	 ArrayList<Integer> employeDejaValider = new ArrayList<Integer>(); 
	
	protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_selectionhoraire);
	        
	        Intent intent =getIntent();
	        idChef = intent.getLongExtra("idChef", 0);
	        idChant = intent.getLongExtra("idChant", 0);
	        
	        //On instancie le calendar pour les timePickers
	        Calendar c=Calendar.getInstance();
	        hour=c.get(Calendar.HOUR);
	        min=c.get(Calendar.MINUTE);
	        
	        timePickerDialog = new CustomTimePickerDialog(this, timeSetListener,timeSetListenerAucun, hour, min, true);
	                
	        //On recupère la date du jour
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	        dateOfDay = format.format(c.getTime());
	        
	        //On récupère le textView du Header pour afficher l'employé séléctionné
	        tv = (TextView) findViewById(R.id.workerTitle);
	        
	        //On recupère les employés  séléctionnés
	        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
	        listWorker = dw.getWorkers();
	        
	        loadModel = intent.getIntExtra("loadModel", 0);
	        if(loadModel==1){
				derniereFiche = intent.getStringExtra("derniereFiche");
			}
	        
	        //Pour chaque travailleur, on créer une fiche
	        for(int i=0; i <listWorker.size(); i++){  	   
        		long id = listWorker.get(i).getId(); 	        
        		Fiche fiche = new Fiche("", 0, 0, id, "", "", "", "", false, false, false);
        		listeFiches.add(fiche);	        		
	        } 
	        turn();
	 }
	
	public void turn(){
        tv.setText(listWorker.get(incredEmploy).getName());
        ficheEnCour = listeFiches.get(incredEmploy);
        ficheEnCour.setJour(dateOfDay);
        ficheEnCour.setChef_id(idChef); 
        ficheEnCour.setChanter_id(idChant); 
        
        if(loadModel==1 && !employeDejaValider.contains(incredEmploy)){
        	generateData();	
        }
        initView();
	}
	
	public void initView(){
		TextView tv1 = (TextView) findViewById(DEB_MAT_ID);
		TextView tv2 = (TextView) findViewById(FIN_MAT_ID);
		TextView tv3 = (TextView) findViewById(DEB_APR_ID);
		TextView tv4 = (TextView) findViewById(FIN_APR_ID);
		Button precButton = (Button) findViewById(R.id.precButton);
		if(incredEmploy==0){
		    precButton.setVisibility(View.GONE);
		}else{
			precButton.setVisibility(View.VISIBLE);
		}
		
		if(loadModel==1){
			
			//deb matin
			if(ficheEnCour.getMatin_deb().length()>=16)
				h1 = ficheEnCour.getMatin_deb().substring(11, 16);
 	        else
 	        	h1 ="";
			
			//fin matin
			if(ficheEnCour.getMatin_fin().length()>=16)
				h2 = ficheEnCour.getMatin_fin().substring(11, 16);
 	        else
 	        	h2 ="";
			
			//deb aprem
			if(ficheEnCour.getAprem_deb().length()>=16)
				h3 = ficheEnCour.getAprem_deb().substring(11, 16);
 	        else
 	        	h3 ="";
			
			//fin aprem
			if(ficheEnCour.getAprem_fin().length()>=16)
				h4 = ficheEnCour.getAprem_fin().substring(11, 16);
 	        else
 	        	h4 ="";
			
		}
		
		if(h1.equals("")){
			tv1.setText("Aucun");
		}else{
			tv1.setText(h1);
		}
		
		if(h2.equals("")){
			tv2.setText("Aucun");
		}else{
			tv2.setText(h2);
		}
		
		if(h3.equals("")){
			tv3.setText("Aucun");
		}else{
			tv3.setText(h3);
		}
		
		if(h4.equals("")){
			tv4.setText("Aucun");
		}else{
			tv4.setText(h4);
		}
	}
	
	 public void Valider(View v) {
			
		 // On modifie les fiches avec les valeurs quand on clique sur valider, peu importe si on a changé l'heure ou non
		 if(checkValue()){
			 if(h1==null || h1.isEmpty() || h1.equals("null") ) {
				 ficheEnCour.setMatin_deb("");
			 }
			 else {
				 ficheEnCour.setMatin_deb(ficheEnCour.getJour() + " " + h1 + ":00");
			 }

			 if(h2==null || h2.isEmpty() || h2.equals("null") ) {
				 ficheEnCour.setMatin_fin("");
			 } else {
				 ficheEnCour.setMatin_fin(ficheEnCour.getJour() + " " + h2 + ":00");
			 }

			 if(h3==null || h3.isEmpty() || h3.equals("null") ) {
				 ficheEnCour.setAprem_deb("");
			 }
			 else {
				 ficheEnCour.setAprem_deb(ficheEnCour.getJour() + " " + h3 + ":00");
			 }

			 if(h4==null || h4.isEmpty() || h4.equals("null") ) {
				 ficheEnCour.setAprem_fin("");
			 }
			 else {
				 ficheEnCour.setAprem_fin(ficheEnCour.getJour() + " " + h4 + ":00");
			 }
			 
			 if(!employeDejaValider.contains(incredEmploy)){
				 employeDejaValider.add(incredEmploy);
			 }
			 
			 
			 incredEmploy++;	 

			 if(incredEmploy<listWorker.size()){
				 turn();
			 }
			 else{
				 Intent goToSelectionTrajet = new Intent (v.getContext(),TrajetActivity.class);
				 goToSelectionTrajet.putExtra("listFiches", new FicheSerializable(listeFiches));
				 goToSelectionTrajet.putExtra("listWorker", new DataWrapper(listWorker));
				 goToSelectionTrajet.putExtra("derniereFiche", derniereFiche);
				 goToSelectionTrajet.putExtra("loadModel", loadModel);
				 startActivity(goToSelectionTrajet);
			 }
		 }
		 
	}
	
	 public void Prec(View v) {
			
		 // On modifie les fiches avec les valeurs quand on clique sur valider, peu importe si on a changé l'heure ou non

		 if(h1==null || h1.isEmpty() || h1.equals("null") ) {
			 ficheEnCour.setMatin_deb("");
		 }
		 else {
			 ficheEnCour.setMatin_deb(ficheEnCour.getJour() + " " + h1 + ":00");
		 }

		 if(h2==null || h2.isEmpty() || h2.equals("null") ) {
			 ficheEnCour.setMatin_fin("");
		 } else {
			 ficheEnCour.setMatin_fin(ficheEnCour.getJour() + " " + h2 + ":00");
		 }

		 if(h3==null || h3.isEmpty() || h3.equals("null") ) {
			 ficheEnCour.setAprem_deb("");
		 }
		 else {
			 ficheEnCour.setAprem_deb(ficheEnCour.getJour() + " " + h3 + ":00");
		 }

		 if(h4==null || h4.isEmpty() || h4.equals("null") ) {
			 ficheEnCour.setAprem_fin("");
		 }
		 else {
			 ficheEnCour.setAprem_fin(ficheEnCour.getJour() + " " + h4 + ":00");
		 }

		 incredEmploy--;	 

		 if(incredEmploy<listWorker.size()){
			 turn();
		 }
		 
	} 
	
	public void showTimeDialog(View v){
		idSelected=v.getId();
		int hourToSelect = 0;
		int minToSelect = 0;
		switch(idSelected){
	    	case DEB_MAT_ID:
	    		if(!h1.equals("")){
	    			hourToSelect = Integer.parseInt(h1.substring(0, 2));
	    			minToSelect = Integer.parseInt(h1.substring(3, 5))/15;
	    		}else{
	    			hourToSelect = 8;
	    		}
	    	break;
	    	case FIN_MAT_ID:
	    		if(!h2.equals("")){
	    			hourToSelect = Integer.parseInt(h2.substring(0, 2));
	    			minToSelect = Integer.parseInt(h2.substring(3, 5))/15;
	    		}else{
	    			hourToSelect = 12;
	    		}
		    break;
	    	case DEB_APR_ID:
	    		if(!h3.equals("")){
	    			hourToSelect = Integer.parseInt(h3.substring(0, 2));
	    			minToSelect = Integer.parseInt(h3.substring(3, 5))/15;
	    		}else{
	    			hourToSelect = 14;
	    		}
		    break;
	    	case FIN_APR_ID:
	    		if(!h4.equals("")){
	    			hourToSelect = Integer.parseInt(h4.substring(0, 2));
	    			minToSelect = Integer.parseInt(h4.substring(3, 5))/15;
	    		}else{
	    			hourToSelect = 18;
	    		}
		    break;
		}
		timePickerDialog.updateTime(hourToSelect, minToSelect);
	    showDialog(TIME_DIALOG_ID);
	}
	 
	protected Dialog onCreateDialog(int id){
	    switch(id){
	    	case TIME_DIALOG_ID:
	    		return timePickerDialog;
	    }
	    return null;
	}
	 
	private TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
			@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			
			hour=hourOfDay;
			min=minute;
			
			String minString  =String.valueOf(min) ;
			String hourString =String.valueOf(hour);
		
			if(minString.length()==1){
				minString ="0"+minString;
			}
			
			if(hourString.length()==1){
				hourString ="0"+hourString;
			}
			
			String time = hourString+":"+minString;
			
			TextView label = (TextView) findViewById(idSelected);
			label.setText(time);
			
			 switch(idSelected){
		    	case DEB_MAT_ID:
		    		// On définit l'heure de la variable correspondante
		    		h1 = time;
		    	break;
		    	case FIN_MAT_ID:
		    		// On définit l'heure de la variable correspondante
		    		h2 = time;
			    break;
		    	case DEB_APR_ID:
		    		// On définit l'heure de la variable correspondante
		    		h3 = time;
			    break;
		    	case FIN_APR_ID:
		    		// On définit l'heure de la variable correspondante
		    		h4 = time;
			    break;
		    }
			 
			

		}
	};
	
	private TimePickerDialog.OnTimeSetListener timeSetListenerAucun=new TimePickerDialog.OnTimeSetListener() {
			@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			
			TextView label = (TextView) findViewById(idSelected);
			label.setText("Aucun");
			
			 switch(idSelected){
		    	case DEB_MAT_ID:
		    		// On définit l'heure de la variable correspondante
		    		h1 = "";
		    	break;
		    	case FIN_MAT_ID:
		    		// On définit l'heure de la variable correspondante
		    		h2 = "";
			    break;
		    	case DEB_APR_ID:
		    		// On définit l'heure de la variable correspondante
		    		h3 = "";
			    break;
		    	case FIN_APR_ID:
		    		// On définit l'heure de la variable correspondante
		    		h4 = "";
			    break;
		    }
			 
			
	
		}
	};
	      
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
	}
	
	private void generateData(){
		
        try {
        	
        	JSONObject jsonFiche = new JSONObject(derniereFiche);
			JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
			
			for (int i = 0; i < details.length(); ++i) {
				
			    JSONObject detail = details.getJSONObject(i);
			    JSONObject employeDetail = detail.getJSONObject("Employe");
			    JSONObject ficheDetail = detail.getJSONObject("Fiche");
			    
			    String name = employeDetail.getString("nom")+" "+employeDetail.getString("prenom");
			    
			    if(name.equals(listWorker.get(incredEmploy).getName())){
			    	
			    	//Get matin_debut
		 	        String matin_debut = ficheDetail.getString("matin_debut");
		 	        ficheEnCour.setMatin_deb(matin_debut);
		 	       
		 	        //get matin_fin
		 	        String matin_fin = ficheDetail.getString("matin_fin");
		 	        ficheEnCour.setMatin_fin(matin_fin);
		 	        
		 	        //get aprem_debut
		 	        String aprem_debut = ficheDetail.getString("aprem_debut");
		 	       	ficheEnCour.setAprem_deb(aprem_debut);
		 	       	
		 	       	//get aprem_fin
		 	        String aprem_fin = ficheDetail.getString("aprem_fin");
		 	      	ficheEnCour.setAprem_fin(aprem_fin);
			    	
		 	      	if(ficheEnCour.getMatin_deb().length()>=16)
		 	      		matin_debut = ficheEnCour.getMatin_deb().substring(11, 16);
		 	      	
		 	      	if(ficheEnCour.getMatin_fin().length()>=16)
		 	      		matin_fin = ficheEnCour.getMatin_fin().substring(11, 16);
		 	      	
		 	      	if(ficheEnCour.getAprem_deb().length()>=16)
		 	      		aprem_debut = ficheEnCour.getAprem_deb().substring(11, 16);
		 	      	
		 	      	if(ficheEnCour.getAprem_fin().length()>=16)
		 	      		aprem_fin = ficheEnCour.getAprem_fin().substring(11, 16);
		 	      	
		 	      	if(matin_debut.equals("01:00") && matin_fin.equals("01:00")){
			 	        ficheEnCour.setMatin_deb("");
			 	        ficheEnCour.setMatin_fin("");
		 	      	}
		 	      	
		 	      	if(aprem_debut.equals("01:00") && aprem_fin.equals("01:00")){
			 	        ficheEnCour.setAprem_deb("");
			 	        ficheEnCour.setAprem_fin("");
		 	      	}
		 	      	
			    }
			    
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
	
	private Boolean checkValue(){
		//creation dialog message erreur
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		String title = "Erreur lors de la selection des horaires";
		AlertDialog dialog;
		
		int h1H = 0, h1M = 0, h2H = 0, h2M = 0, h3H = 0, h3M = 0, h4H = 0, h4M = 0;
		
    	if(!h1.equals("")){
    		h1H = Integer.parseInt(h1.substring(0, 2));
    		h1M = Integer.parseInt(h1.substring(3, 5));
    	}
    	
    	if(!h2.equals("")){
    		h2H = Integer.parseInt(h2.substring(0, 2));
    		h2M = Integer.parseInt(h2.substring(3, 5));
    	}
    	
    	if(!h3.equals("")){
    		h3H = Integer.parseInt(h3.substring(0, 2));
    		h3M = Integer.parseInt(h3.substring(3, 5));
    	}
    	
    	if(!h4.equals("")){
    		h4H = Integer.parseInt(h4.substring(0, 2));
    		h4M = Integer.parseInt(h4.substring(3, 5));
    	}
    	
    	//cas erreur si les deux plages sont nuls
    	if(h1.equals("") && h2.equals("") && h3.equals("") && h4.equals("")){
    		builder.setMessage("Veuillez renseigner au moins une plage horaire").setTitle(title);
			dialog = builder.create();
			dialog.show();
			return false;
    	}
    	
		//cas erreur si matin debut ou matin fin est nul mais pas les deux
		if(h1.equals("")^h2.equals("")){
			builder.setMessage("La plage horaire du matin est incomplète").setTitle(title);
			dialog = builder.create();
			dialog.show();
			return false;
		}
		
		//cas erreur si aprem debut ou aprem fin est nul mais pas les deux 
		if(h3.equals("")^h4.equals("")){
			builder.setMessage("La plage horaire de l'après-midi est incomplète").setTitle(title);
			dialog = builder.create();
			dialog.show();
			return false;
		}
		
		//cas erreur si matin debut > matin fin
		if((h1H>h2H) || ((h1H==h2H) && (h1M>h2M))){
			builder.setMessage("Matin début est supérieur à matin fin").setTitle(title);
			dialog = builder.create();
			dialog.show();
			return false;
		}
		
		//cas erreur si matin fin > aprem debut
		if(!h3.equals("")){
			if((h2H>h3H) || ((h2H==h3H) && (h2M>h3M))){
				builder.setMessage("Attention, la fin de la matiné est supérieur au début de l'après-midi").setTitle(title);
				dialog = builder.create();
				dialog.show();
				return false;
			}
		}
		
		
		//cas erreur si aprem debut > aprem fin
		if((h3H>h4H) || ((h3H==h4H) && (h3M>h4M))){
			builder.setMessage("Attention, le début de l'après midi est supérieur à la fin").setTitle(title);
			dialog = builder.create();
			dialog.show();
			return false;
		}
		
		return true;
	}
}
