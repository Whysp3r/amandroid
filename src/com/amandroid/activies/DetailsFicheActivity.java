package com.amandroid.activies;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.async_task.JSONTransmitter;
import com.amandroid.database.FicheDB; 
import com.amandroid.models.DetailsFicheItem;
import com.amandroid.models.Fiche;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.serializables.FicheSerializable;
import com.amandroid.serializers.FicheSerializer;
import com.amandroid.adapter.DetailsFicheAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class DetailsFicheActivity extends Activity {

		Menu menu;
		
		LinearLayout ll;
		ListView listView;

		String fiche;
		
		@Override
	    protected void onCreate(Bundle savedInstanceState) {
			
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_fichedetails);
	        ll = (LinearLayout) findViewById(R.id.ll);
	        listView = (ListView) findViewById(R.id.listView);
	        
	        Intent intent = getIntent();
	        
	        //On récupère les infos de la fiche que l'on veut afficher
			fiche = intent.getStringExtra("fiche");
			
			// 1. crétion liste des items de la list
			ArrayList<DetailsFicheItem> items =  generateData();
			// 2. crétion de l'adapter
			DetailsFicheAdapter adapter = new DetailsFicheAdapter(this, items);
	 
	        //3. setListAdapter
	        listView.setAdapter(adapter);
	    }
	 
	    private ArrayList<DetailsFicheItem> generateData(){
	        ArrayList<DetailsFicheItem> items = new ArrayList<DetailsFicheItem>();
	        try {
	        	
				JSONObject jsonFiche = new JSONObject(fiche);
				JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
				
				for (int i = 0; i < details.length(); ++i) {
					
				    JSONObject detail = details.getJSONObject(i);
				    JSONObject ficheDetail = detail.getJSONObject("Fiche");
				    JSONObject employeDetail = detail.getJSONObject("Employe");
				    
				    DetailsFicheItem item = new DetailsFicheItem();
				    
				    item.setNomPrenom(employeDetail.getString("nom")+" "+employeDetail.getString("prenom"));
		 	       
				    //Get matin_debut
		 	        String matin_debut = ficheDetail.getString("matin_debut");
		 	        if(matin_debut.length()>=16)
		 	        	matin_debut = matin_debut.substring(11, 16);
		 	        else
		 	        	matin_debut ="";
		 	        item.setMatin_debut(matin_debut);
		 	        
		 	        //get matin_fin
		 	        String matin_fin = ficheDetail.getString("matin_fin");
		 	        if(matin_fin.length()>=16)
		 	        	matin_fin = matin_fin.substring(11, 16);
		 	        else
		 	        	matin_fin ="";
		 	       item.setMatin_fin(matin_fin);
		 	        
		 	        //get aprem_debut
		 	        String aprem_debut = ficheDetail.getString("aprem_debut");
		 	       	if(aprem_debut.length()>=16)
		 	       		aprem_debut = aprem_debut.substring(11, 16);
		 	        else
		 	        	aprem_debut ="";
		 	       	item.setAprem_debut(aprem_debut);
		 	       	
		 	       	//get aprem_fin
		 	        String aprem_fin = ficheDetail.getString("aprem_fin");
		 	      	if(aprem_fin.length()>=16)
		 	      		aprem_fin = aprem_fin.substring(11, 16);
		 	        else
		 	        	aprem_fin ="";
		 	      	item.setAprem_fin(aprem_fin);
		 	        
		 	       
		 	      	//granddeplacement
		 	      	item.setGranddeplacement(ficheDetail.getBoolean("granddeplacement"));
		 	       
		 	      	//repas
		 	      	item.setRepas(ficheDetail.getBoolean("repas"));
		 	        
		 	       //trajet
		 	       item.setTrajet(ficheDetail.getBoolean("trajet"));
		 	       
		 	       items.add(item);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        return items;
	    }
			
			
			
		
		
		

}
