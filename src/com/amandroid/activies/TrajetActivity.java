package com.amandroid.activies;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.models.Fiche;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.serializables.FicheSerializable;
import com.amandroid.adapter.TrajetAdapter;
import com.amandroid.adapter.WorkerAdapter;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

public class TrajetActivity extends ListActivity {

	 Menu menu;
	 ArrayList<Fiche> listFiches = new ArrayList<Fiche>();
	 ArrayList<Worker> listWorkers = new ArrayList<Worker>(); 
	 int loadModel;
	 String derniereFiche; 
	 
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_trajet);
	        
	        //On recupère les employés  séléctionnés
	        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
	        listWorkers = dw.getWorkers();
	        
	        //On recupère les fiches  crées
	        FicheSerializable fs = (FicheSerializable) getIntent().getSerializableExtra("listFiches");
	        listFiches = fs.getFiches();
	        
	        ArrayAdapter<Worker> adapter = new TrajetAdapter(this,listWorkers);
	        this.setListAdapter(adapter); 
	        
	        loadModel = getIntent().getIntExtra("loadModel", 0);
	        if(loadModel==1){
				derniereFiche = getIntent().getStringExtra("derniereFiche");
				generateData();
			}
	        	        
	 }
	 
	 public void Valider(View v) {
	     
	        for(int i=0; i<listWorkers.size();i++){
	        	if(listWorkers.get(i).isSelected()){       		
	        		listFiches.get(i).setTrajet(true);
	        	}
	        	else{
	        		listFiches.get(i).setTrajet(false);
	        	}
	        }
	        
	        Intent goToSelectionRepas = new Intent (v.getContext(),RepasGdActivity.class);
	        goToSelectionRepas.putExtra("listFiches", new FicheSerializable(listFiches));
	        goToSelectionRepas.putExtra("listWorker", new DataWrapper(listWorkers));
	        goToSelectionRepas.putExtra("derniereFiche", derniereFiche);
	        goToSelectionRepas.putExtra("loadModel", loadModel);
	        startActivity(goToSelectionRepas);
	 } 
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	    
	    private void generateData(){
			
	        try {
	        	
	        	JSONObject jsonFiche = new JSONObject(derniereFiche);
				JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
				
				for (int i = 0; i < details.length(); ++i) {
					
				    JSONObject detail = details.getJSONObject(i);
				    JSONObject employeDetail = detail.getJSONObject("Employe");
				    JSONObject ficheDetail = detail.getJSONObject("Fiche");
				    
				    String name = employeDetail.getString("nom")+" "+employeDetail.getString("prenom");
				    
				    for(int j=0; j<listWorkers.size();j++){
				    	
				    	if(name.equals(listWorkers.get(j).getName()) && ficheDetail.getBoolean("trajet")){
				    		
				    		Worker element = listWorkers.get(j);
			        		element.setSelected(true);
			        		listWorkers.remove(element);
			            	listWorkers.add(0,element);
				    		
				    	}
				    	
			        }
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    }

}
