package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import com.amandroid.adapter.AdminAdapter;
import com.amandroid.database.EmployeDB;
import com.amandroid.models.Employe;
import com.amandroid.models.Worker;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.InterimDB;
import com.amandroid.models.Interim;

public class AdminInterimActivity extends Activity {
	

	Menu menu;

	ListView listView;
	ArrayAdapter<Worker> listViewAdapter;
	Long idEmploye;
	ArrayList<Worker> listWorker = new ArrayList<Worker>();
	Vector<Interim> interimVector = new Vector<Interim>();

	// Search EditText
	EditText inputSearch;
	ArrayAdapter<String> adapter;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admininterim);

		//On recupère la listView
		listView = (ListView) findViewById(R.id.listView);
		listViewAdapter = new AdminAdapter(this,getWorker());
		listView.setAdapter(listViewAdapter);

		TextWatcher filterTextWatcher = new TextWatcher() {

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String searchString = s.toString();
				ArrayList<Worker> workerTemplist = new ArrayList<Worker>();
				for (int i = 0; i < listWorker.size(); i++){
					String name = listWorker.get(i).getName();

					if (name.toLowerCase().contains(s)){
						workerTemplist.add(listWorker.get(i));
					}
				}
				listViewAdapter = new AdminAdapter(AdminInterimActivity.this,workerTemplist);
				listView.setAdapter(listViewAdapter);
			}
		};
		inputSearch = (EditText) findViewById(R.id.inputSearch);
		inputSearch.addTextChangedListener(filterTextWatcher);

		listView.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Worker workerSelected = (Worker)listViewAdapter.getItem(position);
				idEmploye = workerSelected.getId();

				//On passe l'id de l'interimaire à l'intent et on lance la nouvelle activité.
				Intent goToFicheDetailEmploye = new Intent (view.getContext(),DetailEmployeActivity.class);
				goToFicheDetailEmploye.putExtra("idEmploye",idEmploye);
				startActivity(goToFicheDetailEmploye);
			}
		});
	}

	private ArrayList<Worker> getWorker() {
		try {
			//Création d'une instance de ma classe EmployeDB
			InterimDB interimBdd = new InterimDB(this);
			JSONArray interimsArray = new JSONArray();
			interimsArray = getInterims();

			interimBdd.open();
			interimVector = interimBdd.getAll();
			interimBdd.close();

			if(interimsArray!=null){
				int nbEmployes = interimsArray.length();

				interimBdd.open();
				interimBdd.deleteAll();

				//On peuple la liste des employes à partir du json object
				for(int i=0; i < nbEmployes; i++){

					JSONObject employeObject = interimsArray.getJSONObject(i);

					String nomEmploye =  employeObject.getJSONObject("Employe").getString("nom");
					String prenomEmploye =  employeObject.getJSONObject("Employe").getString("prenom");
					String idMysql = employeObject.getJSONObject("Employe").getString("id");
					String tel = employeObject.getJSONObject("Employe").getString("tel");
					String add = employeObject.getJSONObject("Employe").getString("adresse");
					String ville = employeObject.getJSONObject("Employe").getString("ville");
					String comm = employeObject.getJSONObject("Employe").getString("commentaire");
					String idAgence = employeObject.getJSONObject("Employe").getString("agence_id");

					//On met à jour la bdd pour la liste des employés
					Interim interim = new Interim(prenomEmploye, nomEmploye, idMysql, tel, add, ville, comm, idAgence);
					interimBdd.addInterim(interim);

					String name = nomEmploye+" "+prenomEmploye;
					listWorker.add(get(name,Long.parseLong(idMysql)));

				}
				interimBdd.close();
			}
			else{
				for(int j =0; j<interimVector.size(); j++){
					String nameEmp = interimVector.get(j).getPrenom()+" "+interimVector.get(j).getNom();
					long id = Long.parseLong( interimVector.get(j).getId_mysql());
					listWorker.add(get(nameEmp,id));
				}
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listWorker;
	}

	private Worker get(String name, long id) {
		return new Worker(name, id);
	}

	public JSONArray getInterims() throws JSONException{
		
		String json =null;
		JSONArray listeInterimArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String urlString = "http://www.alpex-epdm.fr/AM/api/getInterims";			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			listeInterimArray = new JSONArray(json);
        return listeInterimArray;	
		}
		else{
			//TODO
			//Récupèrer la liste des interimaires en local (SQlite)
			listeInterimArray = null;
		return listeInterimArray;	
		}			
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

}
