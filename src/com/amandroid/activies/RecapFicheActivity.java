package com.amandroid.activies;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.Intent;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.async_task.JSONTransmitter;
import com.amandroid.database.FicheDB;
import com.amandroid.models.DetailsFicheItem;
import com.amandroid.models.Fiche;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.serializables.FicheSerializable;
import com.amandroid.serializers.FicheSerializer;
import com.amandroid.adapter.DetailsFicheAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RecapFicheActivity extends Activity {

	Menu menu;

	ArrayList<Fiche> listFiches = new ArrayList<Fiche>();
	ArrayList<Worker> listWorkers = new ArrayList<Worker>();

	LinearLayout ll;
	ListView listView;

	String matin_deb;
	String matin_fin;
	String aprem_deb;
	String aprem_fin;
	int errors = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recapfiche);
		ll = (LinearLayout) findViewById(R.id.ll);
		listView = (ListView) findViewById(R.id.listView);

		//On recupère les employés  séléctionnés
		DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
		listWorkers = dw.getWorkers();

		//On recupère les fiches  crées
		FicheSerializable fs = (FicheSerializable) getIntent().getSerializableExtra("listFiches");
		listFiches = fs.getFiches();

		// 1. crétion liste des items de la list
		ArrayList<DetailsFicheItem> items = generateData();

		// 2. crétion de l'adapter
		DetailsFicheAdapter adapter = new DetailsFicheAdapter(this, items);

		//3. setListAdapter
		listView.setAdapter(adapter);


	}

	private ArrayList<DetailsFicheItem> generateData() {
		ArrayList<DetailsFicheItem> items = new ArrayList<DetailsFicheItem>();
		for (int i = 0; i < listWorkers.size(); i++) {

			DetailsFicheItem item = new DetailsFicheItem();


			item.setNomPrenom(listWorkers.get(i).getName());
			
			if (listFiches.get(i).getMatin_deb() != null && listFiches.get(i).getMatin_deb().length() > 1)
				matin_deb = listFiches.get(i).getMatin_deb().substring(11, 16);
			else
				matin_deb = "00:00";
			item.setMatin_debut(matin_deb);

			if (listFiches.get(i).getMatin_fin() != null && listFiches.get(i).getMatin_fin().length() > 1)
				matin_fin = listFiches.get(i).getMatin_fin().substring(11, 16);
			else
				matin_fin = "00:00";
			item.setMatin_fin(matin_fin);

			if (listFiches.get(i).getAprem_deb() != null && listFiches.get(i).getAprem_deb().length() > 1)
				aprem_deb = listFiches.get(i).getAprem_deb().substring(11, 16);
			else
				aprem_deb = "00:00";
			item.setAprem_debut(aprem_deb);

			if (listFiches.get(i).getAprem_fin() != null && listFiches.get(i).getAprem_fin().length() > 1)
				aprem_fin = listFiches.get(i).getAprem_fin().substring(11, 16);
			else
				aprem_fin = "00:00";
			item.setAprem_fin(aprem_fin);

			item.setGranddeplacement(listFiches.get(i).isGd());

			item.setRepas(listFiches.get(i).isRepas());

			item.setTrajet(listFiches.get(i).isTrajet());

			items.add(item);
		}

		return items;
	}


	public void Envoyer(View v) throws InterruptedException, ExecutionException {

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Fiche.class, new FicheSerializer());
		gsonBuilder.setPrettyPrinting();
		Gson gson = gsonBuilder.create();

		FicheDB ficheBdd = new FicheDB(this);


		for (int i = 0; i < listFiches.size(); i++) {
			String json = gson.toJson(listFiches.get(i));

			try {
				JSONObject toSend = new JSONObject(json);
				JSONTransmitter transmitter = new JSONTransmitter();
				JSONObject response = transmitter.execute(new JSONObject[]{toSend}).get();

				System.out.println("response: "+response);

				if (response == null || response.getString("success").equals("false")) {
					//on ajoute 1 au compteur d'erreur.
					errors = errors + 1;

					//On sauvegarde la fiche en locale
					ficheBdd.open();
					ficheBdd.addFiche(listFiches.get(i));
					ficheBdd.close();
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

		if (errors != 0) {
			new AlertDialog.Builder(this)
					.setTitle("Erreur D'envoie")
					.setMessage(errors+" fiche(s) n'ont pas été envoyée(s)." + System.getProperty("line.separator") + "Elles sont stockées sur le téléphones en attente de connexion")
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent returnToNewFiche = new Intent(listView.getContext(), ListeChantierActivity.class);
							returnToNewFiche.putExtra("idEmploye", listFiches.get(0).getChef_id());
							startActivity(returnToNewFiche);
						}
					}).setIcon(android.R.drawable.ic_dialog_alert).show();
		} else {
			Toast.makeText(this, "Les fiches ont bien été envoyées", Toast.LENGTH_SHORT).show();
			Intent returnToNewFiche = new Intent(listView.getContext(), ListeChantierActivity.class);
			returnToNewFiche.putExtra("idEmploye", listFiches.get(0).getChef_id());
			startActivity(returnToNewFiche);
		}
	}
}