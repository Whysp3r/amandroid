package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.EmployeDB;
import com.amandroid.database.InterimDB;
import com.amandroid.models.Employe;
import com.amandroid.models.Interim;
import com.google.gson.JsonObject;

public class DetailEmployeActivity extends Activity {
	Menu menu;
	JSONObject detailEmploye;
	JSONArray listeAgence;
	
	String name;
	String tel;
	String adresse;
	String ville;
	String commentaire;
	String agenceId;
	String agenceName;
	boolean interim;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailemploye);
        
        Intent intent = getIntent();
        
	    //On récupère l'id de l'empolyé séléctionné
		String idEmploye = String.valueOf(intent.getLongExtra("idEmploye",0)) ;
		
		//On récupère les informations de l'empoyé séléctionné
		try {
			detailEmploye = getDetailEmploye(idEmploye);
			
			if(detailEmploye!=null){
				name = detailEmploye.getJSONObject("Employe").getString("prenom")
						+" "
						+detailEmploye.getJSONObject("Employe").getString("nom");
				tel = detailEmploye.getJSONObject("Employe").getString("tel");
				adresse = detailEmploye.getJSONObject("Employe").getString("adresse");
				ville = detailEmploye.getJSONObject("Employe").getString("ville");
				commentaire = detailEmploye.getJSONObject("Employe").getString("commentaire");
				agenceId = detailEmploye.getJSONObject("Employe").getString("agence_id");
				interim = detailEmploye.getJSONObject("Employe").getBoolean("interim");
				
				if(interim){
					
					listeAgence =getInterims();
					
					int i =0;
					String idAgence="";
					JSONObject interim = new JSONObject();
					
					while(!idAgence.equals(agenceId)){
						 interim = listeAgence.getJSONObject(i);
						 idAgence =  interim.getJSONObject("Agence").getString("id");
						 i++; 
					}
		        	agenceName = interim.getJSONObject("Agence").getString("nom");	        				
				}
			}
			else{
			
				//Création d'une instance de la classe  InterimDB 
			    InterimDB interimBdd = new InterimDB(this);
			     
			    //On récupère la liste des intérimaires en local
			    interimBdd.open();
			    Interim interim = interimBdd.getInterimByIdSql(idEmploye);
				interimBdd.close();
				
				if(interim != null){
									
					name = interim.getPrenom()
							+" "
							+interim.getNom();
					tel = interim.getTel();
					adresse = interim.getAdd();
					ville = interim.getVille();
					commentaire = interim.getCommentaire();	
					
				}
				else{	
					//Création d'une instance de la classe  InterimDB 
				    EmployeDB employeBdd = new EmployeDB(this);
				     
				    //On récupère la liste des intérimaires en local
				    employeBdd.open();
				    Employe employe = employeBdd.getEmployeByIdSql(idEmploye);
				    employeBdd.close();
				    
					name = employe.getPrenom()
							+" "
							+employe.getNom();
					tel = employe.getTel();
					adresse = employe.getAdd();
					ville = employe.getVille();
					commentaire = employe.getCommentaire();	
					
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//On affiche les informations dansla vue
		TextView labNom = (TextView) findViewById(R.id.labelNom);
		labNom.setText(name);
		TextView labTel = (TextView) findViewById(R.id.labelPhone);
		labTel.setText(tel);
		TextView labAdd = (TextView) findViewById(R.id.labelAdresse);
		labAdd.setText(adresse);
		TextView labVille = (TextView) findViewById(R.id.labelVille);
		labVille.setText(ville);
		
		if(interim){
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.label6);			
			TextView labAgence = (TextView) findViewById(R.id.labelAgence);
			rl.setVisibility(0);
			labAgence.setVisibility(0);
			labAgence.setText(agenceName);
		}
		
		TextView labComm = (TextView) findViewById(R.id.labelComm);
		labComm.setText(commentaire);    
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
			}
		return super.onOptionsItemSelected(item);		
	}
	
	public JSONObject getDetailEmploye(String id) throws JSONException{
		
		String json =null;
		JSONObject employe;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String param = id;
			String urlString = "http://www.alpex-epdm.fr/AM/api/getEmployeById/"+param;			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			employe = new JSONObject(json);
        return employe;	
		}
		else{
			//TODO
			//Récupèrer la liste des employés en local (SQlite)
			employe = null;
		return employe;	
		}	
	}
	
	public JSONArray getInterims() throws JSONException{
		
		String json =null;
		JSONArray listeInterimArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String urlString = "http://www.alpex-epdm.fr/AM/api/getInterims";			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			listeInterimArray = new JSONArray(json);
        return listeInterimArray;	
		}
		else{
			//TODO
			//Récupèrer la liste des interimaires en local (SQlite)
			listeInterimArray = null;
		return listeInterimArray;	
		}			
	}

}
