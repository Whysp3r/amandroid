package com.amandroid.activies;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;

import com.amandroid.R;
import com.amandroid.alarm.AlarmReceiver;
import com.amandroid.models.Alarm;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TimePicker;
import android.widget.Toast;

public class SettingsActivity extends Activity implements OnTimeSetListener{
	Menu menu;
    static final int ALARM_ID = 1234567;
    static Alarm alarm;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	   	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        
        //Chargement des informations du reveil
    	charger();
        
        //Affichage 
        affichage();

    }
    private void affichage() {
    	//Ici on a juste voulu créer un affichage de l'heure qui soit au format hh:mm.
		String heureReveil = "";
		heureReveil += alarm.getHeure().hour >=10 ? alarm.getHeure().hour : "0" + alarm.getHeure().hour;
		heureReveil +=":";
		heureReveil += alarm.getHeure().minute >=10 ? alarm.getHeure().minute : "0" + alarm.getHeure().minute;
		CheckBox ck_alarm = (CheckBox)findViewById(R.id.heure);
		ck_alarm.setText(heureReveil);
		ck_alarm.setChecked(alarm.isActive());
	}
	
    /*
     * changeHeure se déclenche automatiquement au click sur l'heure ou la CheckBox.
     * Active ou désactive le reveil.
     * Affiche un dialog pour choisir l'heure de reveil
     */
    public void changeHeure(View target){
		CheckBox ck_alarm = (CheckBox)findViewById(R.id.heure);
		
		//Si on active l'alarm alors on veut choisir l'heure.
		if(ck_alarm.isChecked()){
	    	TimePickerDialog dialog = new TimePickerDialog(this, this, alarm.getHeure().hour, alarm.getHeure().minute, true);
	    	dialog.show();
		}else{
			Toast.makeText(this, "L'alarme a été désactivée" , Toast.LENGTH_SHORT).show();
		}
    }
    
    /*
     * Chargement des informations du reveil.
     * Ici pour la sauvegarde on a simplement déserialiser l'objet Alarm.
     */
    public void charger(){
    	alarm = null;
    	try {
    		ObjectInputStream alarmOIS= new ObjectInputStream(openFileInput("alarm.ser"));   
    		alarm = new Alarm();
    		alarm.setActive(true);
    		
    		Time timeLoaded = new Time();
    		String timetime = alarmOIS.readUTF();
    		timeLoaded.parse(timetime);
    		
    		alarm.setHeure(timeLoaded);
    		alarmOIS.close();
		}
    	catch(FileNotFoundException fnfe){
    		System.out.println("ko");
    		alarm = new Alarm();
        	alarm.setActive(true);
        	Time t = new Time();
        	t.hour = 18;
        	t.minute = 30;
        	alarm.setHeure(t);
    	}
    	catch(IOException ioe) {
			ioe.printStackTrace();
		}
    }   
    
    /*
     * Sauvegarde des informations du reveil
     */
    public void sauver(){
    	try {
    		ObjectOutputStream alarmOOS= new ObjectOutputStream(openFileOutput("alarm.ser",MODE_WORLD_WRITEABLE));
    		
    		String time = alarm.getHeure().toString().substring(0,15);
    		
			alarmOOS.writeUTF(time);
			alarmOOS.flush();
			alarmOOS.close();
		}
    	catch(IOException ioe) {
			ioe.printStackTrace();
		}
    }
	
    /*
     * Est activé après avoir valider l'heure du reveil.
     * En fait on sauvegarde simplement la nouvelle heure. On l'affiche comme il faut et on replanifie le reveil
     */
    @Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		Time t = new Time();
		t.hour = hourOfDay;
		t.minute = minute;
		alarm.setHeure(t);
		affichage();
		planifierAlarm();
	}
	
    /*
	 * Job de planification de l'alarm
	 */
    private void planifierAlarm() {
		//Récupération de l'instance du service AlarmManager.
		AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
		
		//On instancie l'Intent qui va être appelé au moment du reveil.
		Intent intent = new Intent(this, AlarmReceiver.class);
		
		//On créer le pending Intent qui identifie l'Intent de reveil avec un ID et un/des flag(s)
		PendingIntent pendingintent = PendingIntent.getBroadcast(this, ALARM_ID, intent, 0);
		
		//On annule l'alarm pour replanifier si besoin
		am.cancel(pendingintent);
		
		//La on va déclencher un calcul pour connaitre le temps qui nous sépare du prochain reveil.
		Calendar reveil  = Calendar.getInstance();
		reveil.set(Calendar.HOUR_OF_DAY, alarm.getHeure().hour);
		reveil.set(Calendar.MINUTE, alarm.getHeure().minute);
		
		if(reveil.compareTo(Calendar.getInstance()) == -1)
			reveil.add(Calendar.DAY_OF_YEAR, 1);
		
		Calendar cal = Calendar.getInstance();
		reveil.set(Calendar.SECOND, 0);
		cal.set(Calendar.SECOND, 0);
		long diff = reveil.getTimeInMillis() - cal.getTimeInMillis();
		
		//On ajoute le reveil au service de l'AlarmManagerso
		am.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis()+diff, AlarmManager.INTERVAL_DAY, pendingintent);
		
		//On sauvegarde l'alarme
		sauver();
		
		//Affichage de l'alarme programmé (TOAST)
		String timeToast = "";
		timeToast += reveil.get(Calendar.DAY_OF_MONTH) +" à ";
		timeToast += alarm.getHeure().hour >=10 ? alarm.getHeure().hour : "0" + alarm.getHeure().hour;
		timeToast +=":";
		timeToast += alarm.getHeure().minute >=10 ? alarm.getHeure().minute : "0" + alarm.getHeure().minute;
		
		Toast.makeText(this, "Alarme programmé le " +timeToast , Toast.LENGTH_SHORT).show();
	}
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
			}
		return super.onOptionsItemSelected(item);		
	}
}