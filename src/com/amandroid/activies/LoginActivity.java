 package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.async_task.JSONTransmitter;
import com.amandroid.database.ChantierDB;
import com.amandroid.database.ChefDB;
import com.amandroid.database.FicheDB;
import com.amandroid.models.Chef;

import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


import com.amandroid.models.Fiche;
import com.amandroid.serializers.FicheSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity {
	
	//Menu de l'ActionBar
	private Menu menu;
	
	//Création d'une instance de ma classe ChefDB
    ChefDB chefBdd = new ChefDB(this);
	Chef chef = null;

	//Le vector pour les fiches en attente d'envoie
	Vector<Fiche> ficheEnAttente = new Vector<Fiche>();
	int nbFicheSended = 0;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

		//On verifie si des fiches sont en attente d'envoie
		checkFicheEnAttente();

    }
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
			}
		return super.onOptionsItemSelected(item);		
	}
    
	public void Authentification(View view) throws JSONException{
		
	//Recupere les identifiants renseigné par l'utilisateur
		
		EditText loginView = (EditText) findViewById(R.id.login);
		EditText passView = (EditText) findViewById(R.id.password);
    
		String login = loginView.getText().toString();
		String pass = passView.getText().toString();
				
		//Vérifie si les identifiants rensenigné par l'utilisateur sont correctes
		long auth = this.getAuth(login,pass);		
		Intent i = new Intent(this,ListeChantierActivity.class);
		
		if(auth!=0){
			i.putExtra("idEmploye", auth);
			startActivity(i);
		}
		else{
			chefBdd.open();
			chef = chefBdd.getChefByLoginAndPass(login,pass);
			chefBdd.close();
			if(chef!=null){
				i.putExtra("idEmploye", Long.parseLong(chef.getId_mysql()));
				startActivity(i);
			}
			else{
				new AlertDialog.Builder(this)
			    .setTitle("Erreur d'authentification")
			    .setMessage("Vérifiez si vous avez bien renseigné vos identifiants."+System.getProperty("line.separator")+"Sinon Il s'agit de votre première connexion, attendez d'avoir du réseau !")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						//nothing
					}
				})
						.setIcon(android.R.drawable.ic_dialog_alert)
				.show();
			}
		}
	}
	
	private long getAuth (String login, String pass) throws JSONException{
		
		String json =null;
		long idUser = 0;
		HttpGetter httpGetter = new HttpGetter();
		
		
		try {
			String param = login +"/"+pass;
			String urlString = "http://www.alpex-epdm.fr/AM/api/getChef/"+param;
			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		 
		if(!json.isEmpty()){
			
			// On récupère le JSON complet
            JSONObject jsonObject = new JSONObject(json);
            JSONObject obj = new JSONObject(jsonObject.getString("Chef"));
            
            idUser = obj.getLong("id");
            String id = obj.getString("id");
            String username = obj.getString("username");
            String passwordUser = obj.getString("password");
             
            System.out.println("pass: "+passwordUser);
            
            chefBdd.open();
    	    chef = chefBdd.getChefByLoginAndPass(login, pass);
    	    
    	    if(chef == null){
    	    	chef= new Chef(username, pass, id);
    	    	chefBdd.addChef(chef);
    	    }    
    	    chefBdd.close();
     
    	    return idUser;
		}
		return idUser;
	}

	public void checkFicheEnAttente(){

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Fiche.class, new FicheSerializer());
		gsonBuilder.setPrettyPrinting();
		Gson gson = gsonBuilder.create();

		FicheDB ficheBdd = new FicheDB(this);
		ficheBdd.open();
		ficheEnAttente = ficheBdd.getAll();

		if(ficheEnAttente!=null){

			for(int i =0; i<ficheEnAttente.size(); i++){
				String json = gson.toJson(ficheEnAttente.get(i));

				try {
					JSONObject toSend = new JSONObject(json);
					JSONTransmitter transmitter = new JSONTransmitter();
					JSONObject response = transmitter.execute(new JSONObject[]{toSend}).get();

					//Si la fiche à bien été envoyée, on la supprime du tel et on incrémente le compteur
					if (response != null && response.getString("success").equals("true")) {
						ficheBdd.deleteByJourChantierEmploye(
								ficheEnAttente.get(i).getJour(),
								ficheEnAttente.get(i).getChanter_id(),
								ficheEnAttente.get(i).getEmploye_id()
						);
						nbFicheSended = nbFicheSended+1;
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}

			}
			if(nbFicheSended!=0){
				Toast.makeText(this, nbFicheSended+" fiche(s) ont été envoyée(s)", Toast.LENGTH_SHORT).show();
			}
		}
		else{
			Toast.makeText(this, "Aucune fiches en attente", Toast.LENGTH_SHORT).show();
		}
		ficheBdd.close();
	}
}
	