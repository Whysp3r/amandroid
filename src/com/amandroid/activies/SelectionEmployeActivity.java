package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.EmployeDB;
import com.amandroid.models.DetailsFicheItem;
import com.amandroid.models.Employe;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.adapter.WorkerAdapter;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

public class SelectionEmployeActivity extends ListActivity {
	
	 Menu menu;
	 int nbIntervenants =0;
	 long idChef;
	 long idChant;
	 int loadModel;
	 String derniereFiche;
	 
	 ArrayList<Worker> listWorker = new ArrayList<Worker>();
	 Vector<Employe> employeVector = new Vector<Employe>();
	 // Search EditText
	 EditText inputSearch;
	 ArrayAdapter<Worker> adapter;

	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_selectionemploye);
	        
	        //On récupère le nombre d'intervenant séléctionnés précèdemment
	        Intent intent = getIntent();
	        nbIntervenants = intent.getIntExtra("nbIntervenant", 0);
	        idChef = intent.getLongExtra("idChef", 0);
	        idChant = intent.getLongExtra("idChant", 0);
	        
	        loadModel = intent.getIntExtra("loadModel", 0);
	        getWorker();
	        
			if(loadModel==1){
				derniereFiche = intent.getStringExtra("derniereFiche");
				generateData();
			}
	        
	        adapter = new WorkerAdapter(this,listWorker);
	        this.setListAdapter(adapter);  
	        
	        TextWatcher filterTextWatcher = new TextWatcher() {

	            public void afterTextChanged(Editable s) {
	            }

	            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	            }

	            public void onTextChanged(CharSequence cs, int start, int before, int count) {

	                String searchString = cs.toString();
	                if(searchString.length() < 2) {
	                    adapter = new WorkerAdapter(SelectionEmployeActivity.this,listWorker);
	                    SelectionEmployeActivity.this.setListAdapter(adapter);
	                    return;
	                }
	                ArrayList<Worker> workerTemplist = new ArrayList<Worker>();
	                for (int i = 0; i < listWorker.size(); i++)
	                    {
	                    String name = listWorker.get(i).getName();
	                    
	                    if (name.toLowerCase().contains(cs))
	                        {
	                            workerTemplist.add(listWorker.get(i));
	                        }
	                    }
	                adapter = new WorkerAdapter(SelectionEmployeActivity.this,workerTemplist);
	                SelectionEmployeActivity.this.setListAdapter(adapter);
	            }
	        };
	        inputSearch = (EditText) findViewById(R.id.inputSearch);
	        inputSearch.addTextChangedListener(filterTextWatcher);
	 }
	 
	 private void getWorker() {	 
	    try {	
	    	 //Création d'une instance de ma classe EmployeDB
		     EmployeDB employeBdd = new EmployeDB(this);
			 
			 JSONArray EmployesArray = new JSONArray();  
			 EmployesArray = getEmployes();
			 
			 employeBdd.open();
			 employeVector = employeBdd.getAll();
			 employeBdd.close();
	    	
	    	if(EmployesArray!=null){
		        int nbEmployes = EmployesArray.length();
		        
		        employeBdd.open();
		        employeBdd.deleteAll();
		        
		        //On peuple la liste des employes à partir du json object
		        for(int i=0; i < nbEmployes; i++){      
		     
		        	JSONObject employeObject = EmployesArray.getJSONObject(i);
		        	
		            String nomEmploye =  employeObject.getJSONObject("Employe").getString("nom");	  
		            String prenomEmploye =  employeObject.getJSONObject("Employe").getString("prenom");
		            String idMysql = employeObject.getJSONObject("Employe").getString("id");  
		            String tel = employeObject.getJSONObject("Employe").getString("tel");
		            String add = employeObject.getJSONObject("Employe").getString("adresse");
		            String ville = employeObject.getJSONObject("Employe").getString("ville");
		            String comm = employeObject.getJSONObject("Employe").getString("commentaire");
		            String idAgence = employeObject.getJSONObject("Employe").getString("agence_id");
		            
		            //On met à jour la bdd pour la liste des employés
		            Employe employe = new Employe(prenomEmploye, nomEmploye, idMysql, tel, add, ville, comm, idAgence);
		            employeBdd.addEmploye(employe);
		            
		            String name = nomEmploye+" "+prenomEmploye;
		            listWorker.add(get(name,Long.parseLong(idMysql)));
    		            
		        }		 
		        employeBdd.close();
	    	}
	    	else{
	    		for(int j =0; j<employeVector.size(); j++){
			    	   String nameEmp = employeVector.get(j).getPrenom()+" "+employeVector.get(j).getNom();
			    	   long id = Long.parseLong( employeVector.get(j).getId_mysql()); 
			        listWorker.add(get(nameEmp,id));
				}
			}
	    }
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	private Worker get(String name, long id) {
		    return new Worker(name, id);
		  }

	public JSONArray getEmployes() throws JSONException{
			
			String json =null;
			JSONArray listeEmployesArray;
			HttpGetter httpGetter = new HttpGetter();		
			
			try {
				String urlString = "http://www.alpex-epdm.fr/AM/api/getEmployees";			
				URL url = new URL(urlString);
				httpGetter.execute(url);
				json = httpGetter.get();						

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}		
			//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
			if(!json.isEmpty()){		
				listeEmployesArray = new JSONArray(json);
	        return listeEmployesArray;	
			}
			else{
				//TODO
				listeEmployesArray = null;
			return listeEmployesArray;	
			}
			
	}
    public void Valider(View v) {
        Intent goToSelectionInterim = new Intent (v.getContext(),SelectionInterimActivity.class);
        ArrayList<Worker> listWorkerToSend = new ArrayList<Worker>();
        for(int i=0; i<listWorker.size();i++){
        	if(listWorker.get(i).isSelected()){       		
        		String nameWorker = listWorker.get(i).getName();
        		long id = listWorker.get(i).getId();
        		listWorkerToSend.add(get(nameWorker,id));		
        	}
        }      
        goToSelectionInterim.putExtra("nbIntervenant", nbIntervenants);
        goToSelectionInterim.putExtra("idChef", idChef);
        goToSelectionInterim.putExtra("idChant",idChant);
        goToSelectionInterim.putExtra("listWorker", new DataWrapper(listWorkerToSend));
        goToSelectionInterim.putExtra("derniereFiche", derniereFiche);
        goToSelectionInterim.putExtra("loadModel", loadModel);
        
	    startActivity(goToSelectionInterim); 
       }
    
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	    
	    private void generateData(){
	    	ArrayList<String> employeSelected = new ArrayList<String>(); 
	        try {
	        	
	        	JSONObject jsonFiche = new JSONObject(derniereFiche);
				JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
				
				for (int i = 0; i < details.length(); ++i) {
					
				    JSONObject detail = details.getJSONObject(i);
				    JSONObject employeDetail = detail.getJSONObject("Employe");
				    employeSelected.add(employeDetail.getString("nom")+" "+employeDetail.getString("prenom"));
				    
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       
	        for (int i = 0; i < listWorker.size(); i++) {
	        	
	        	if(employeSelected.contains(listWorker.get(i).getName())){
	        		Worker element = listWorker.get(i);
	        		element.setSelected(true);
	        		listWorker.remove(element);
	            	listWorker.add(0,element);
	        		
	        	}
			}
	    }

}
