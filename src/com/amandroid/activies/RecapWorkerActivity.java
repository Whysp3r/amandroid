package com.amandroid.activies;

import java.util.ArrayList;

import com.amandroid.R;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class RecapWorkerActivity extends Activity {
	ListView listView ;
	ArrayList<Worker> listWorker = new ArrayList<Worker>();
	Menu menu;
	long idChef;
	long idChant;
	int loadModel;
	String derniereFiche;

	 protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_recapworker);
	        
	      //On recupère  la listView
			listView = (ListView) findViewById(R.id.listRecapWorker);
	        
	        //On recupère les employés  séléctionnés
			
			Intent intent = getIntent();
			idChef = intent.getLongExtra("idChef", 0);
			idChant = intent.getLongExtra("idChant", 0);
			
			loadModel = intent.getIntExtra("loadModel", 0);
		    if(loadModel==1){
		    	derniereFiche = intent.getStringExtra("derniereFiche");
			}
			
	        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
	        listWorker = dw.getWorkers();
	        int nbWorker = listWorker.size();
	        String[] workers;
	        workers = new String[nbWorker];
	        for(int i=0; i<nbWorker; i++){     	
	        		String nameWorker = listWorker.get(i).getName();
	        		long id = listWorker.get(i).getId();        		
	        		workers[i]=nameWorker;
	        		System.out.println("this is "+nameWorker+" --> "+id);
	        		System.out.println("worker: "+workers[i]);
	        }
	        //On créer l'adapter et on lui assigne la list view	 
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					 android.R.layout.activity_list_item, android.R.id.text1, workers);
			listView.setAdapter(adapter);
	 }
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	 public void Valider(View v) {
	      Intent goToSelectionHoraire = new Intent (v.getContext(),SelectionHoraireActivity.class);
	      goToSelectionHoraire.putExtra("listWorker", new DataWrapper(listWorker));
	      goToSelectionHoraire.putExtra("idChef", idChef);
	      goToSelectionHoraire.putExtra("idChant", idChant);
	      goToSelectionHoraire.putExtra("derniereFiche", derniereFiche);
	      goToSelectionHoraire.putExtra("loadModel", loadModel);
	      startActivity(goToSelectionHoraire);
	  
	      
	}
}
