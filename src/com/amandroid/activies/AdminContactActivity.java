package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import com.amandroid.adapter.AdminAdapter;
import com.amandroid.database.ContactDB;
import com.amandroid.database.EmployeDB;
import com.amandroid.models.Contact;
import com.amandroid.models.Employe;
import com.amandroid.models.Worker;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;

public class AdminContactActivity extends Activity {

	Menu menu;

	Long idEmploye;
	ListView listView;
	ArrayAdapter<Worker> listViewAdapter;
	ArrayList<Worker> listWorker = new ArrayList<Worker>();

	Vector<Contact> employeVector = new Vector<Contact>();

	//Search EditText
	EditText inputSearch;
	ArrayAdapter<String> adapter;

	/*
	*

	String nomContact =  employeObject.getJSONObject("Contact").getString("nom");
   		            String prenomContact =  employeObject.getJSONObject("Contact").getString("prenom");
   		            String contactId =  employeObject.getJSONObject("Contact").getString("id");
   		            String statutContact = employeObject.getJSONObject("Contact").getString("statut");
	*/

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admincontact);

		//On recupère la listView
		listView = (ListView) findViewById(R.id.listView);
		listViewAdapter = new AdminAdapter(this,getWorker());
		listView.setAdapter(listViewAdapter);

		TextWatcher filterTextWatcher = new TextWatcher() {

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String searchString = s.toString();
				ArrayList<Worker> workerTemplist = new ArrayList<Worker>();
				for (int i = 0; i < listWorker.size(); i++){
					String name = listWorker.get(i).getName();

					if (name.toLowerCase().contains(s)){
						workerTemplist.add(listWorker.get(i));
					}
				}
				listViewAdapter = new AdminAdapter(AdminContactActivity.this,workerTemplist);
				listView.setAdapter(listViewAdapter);
			}
		};
		inputSearch = (EditText) findViewById(R.id.inputSearch);
		inputSearch.addTextChangedListener(filterTextWatcher);

		listView.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Worker workerSelected = (Worker)listViewAdapter.getItem(position);
				idEmploye = workerSelected.getId();

				//On passe l'id du chantier à l'intent et on lance la nouvelle activité.
				Intent goToFicheDetailEmploye = new Intent (view.getContext(),DetailContactActivity.class);
				goToFicheDetailEmploye.putExtra("idEmploye",idEmploye);
				startActivity(goToFicheDetailEmploye);
			}
		});
	}

	private Worker get(String name, long id) {
		return new Worker(name, id);
	}

	private ArrayList<Worker> getWorker() {
		try {
			//Création d'une instance de ma classe EmployeDB
			ContactDB contactBDD = new ContactDB(this);

			JSONArray EmployesArray = new JSONArray();
			EmployesArray = getContacts();

			contactBDD.open();
			employeVector = contactBDD.getAll();
			contactBDD.close();

			if(EmployesArray!=null){
				int nbEmployes = EmployesArray.length();

				contactBDD.open();
				contactBDD.deleteAll();

				//On peuple la liste des employes à partir du json object
				for(int i=0; i < nbEmployes; i++){

					JSONObject employeObject = EmployesArray.getJSONObject(i);

					String nomEmploye =  employeObject.getJSONObject("Contact").getString("nom");
					String prenomEmploye =  employeObject.getJSONObject("Contact").getString("prenom");
					String idMysql = employeObject.getJSONObject("Contact").getString("id");
					String tel = employeObject.getJSONObject("Contact").getString("tel");
					String add = employeObject.getJSONObject("Contact").getString("adresse");
					String ville = employeObject.getJSONObject("Contact").getString("ville");
					String comm = employeObject.getJSONObject("Contact").getString("commentaire");
					String statut = employeObject.getJSONObject("Contact").getString("statut");
					String email = employeObject.getJSONObject("Contact").getString("email");

					//On met à jour la bdd pour la liste des employés
					Contact contact = new Contact(prenomEmploye, nomEmploye, idMysql, tel, add, ville, comm, statut,email);
					contactBDD.addContact(contact);

					String name = nomEmploye+" "+prenomEmploye;
					listWorker.add(get(name,Long.parseLong(idMysql)));

				}
				contactBDD.close();
			}
			else{
				for(int j =0; j<employeVector.size(); j++){
					String nameEmp = employeVector.get(j).getPrenom()+" "+employeVector.get(j).getNom();
					long id = Long.parseLong( employeVector.get(j).getId_mysql());
					listWorker.add(get(nameEmp,id));
				}
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listWorker;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
			}
		return super.onOptionsItemSelected(item);		
	}
	
	public JSONArray getContacts() throws JSONException{
		
		String json =null;
		JSONArray listeContactArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String urlString = "http://www.alpex-epdm.fr/AM/api/getContactAdmin";			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			listeContactArray = new JSONArray(json);
        return listeContactArray;	
		}
		else{
			//TODO
			//Récupèrer la liste des interimaires en local (SQlite)
			listeContactArray = null;
		return listeContactArray;	
		}			
	}

}
