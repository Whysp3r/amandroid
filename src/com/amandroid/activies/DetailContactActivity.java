package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import com.amandroid.database.ContactDB;
import com.amandroid.database.EmployeDB;
import com.amandroid.models.Contact;
import com.amandroid.models.Employe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;

public class DetailContactActivity extends Activity {
	
	Menu menu;
	String idContact;
	JSONArray listeContact;
	
	//variable d'un contact
	
	String nom;
	String prenom;
	String email;
	String tel;
	String statut;
	String add;
	String ville;
	String commentaire;
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailcontact);
        
        Intent intent = getIntent();
		String idContact = String.valueOf(intent.getLongExtra("idEmploye",0)) ;
        
        String name ="";
        
        try {
			listeContact = getContacts();

			if(listeContact!=null) {

				int i = 0;
				JSONObject contact = new JSONObject();
				String contactId = "";

				while (!idContact.equals(contactId)) {
					contact = listeContact.getJSONObject(i);
					contactId = contact.getJSONObject("Contact").getString("id");
					i++;
				}


				if (contact != null) {
					nom = contact.getJSONObject("Contact").getString("nom");
					prenom = contact.getJSONObject("Contact").getString("prenom");
					name = prenom + " " + nom;
					email = contact.getJSONObject("Contact").getString("email");
					tel = contact.getJSONObject("Contact").getString("tel");
					statut = contact.getJSONObject("Contact").getString("statut");
					add = contact.getJSONObject("Contact").getString("adresse");
					ville = contact.getJSONObject("Contact").getString("ville");
					commentaire = contact.getJSONObject("Contact").getString("commentaire");
				}
			}
			else{
				//Création d'une instance de la classe  InterimDB
				ContactDB contactBdd = new ContactDB(this);

				//On récupère la liste des intérimaires en local
				contactBdd.open();
				Contact contactObject = contactBdd.getContactByIdSql(idContact);
				contactBdd.close();

				name = contactObject.getPrenom()
						+" "
						+contactObject.getNom();
				email = contactObject.getEmail();
				statut = contactObject.getStatut();
				tel = contactObject.getTel();
				add = contactObject.getAdd();
				ville = contactObject.getVille();
				commentaire = contactObject.getCommentaire();

			}
	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    	//On affiche les informations dans la vue
		TextView labNom = (TextView) findViewById(R.id.labelNom);
		labNom.setText(name);
		TextView labStatut = (TextView) findViewById(R.id.labelStatut);
		labStatut.setText(statut);
		TextView labMail = (TextView) findViewById(R.id.labelMail);
		labMail.setText(email);
		TextView labTel = (TextView) findViewById(R.id.labelPhone);
		labTel.setText(tel);
		TextView labAdd = (TextView) findViewById(R.id.labelAdresse);
		labAdd.setText(add);
		TextView labVille = (TextView) findViewById(R.id.labelVille);
		labVille.setText(ville);
		TextView labComm = (TextView) findViewById(R.id.labelComm);
		labComm.setText(commentaire);    

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		this.menu = menu;
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {	
			case R.id.action_administration:
				Intent goToAdministration = new Intent(this,AdministrationActivity.class);
				startActivity(goToAdministration);
				break;
			case R.id.action_fiche:
				Intent goToFiche = new Intent(this,LoginActivity.class);
				startActivity(goToFiche);
				break;
			case R.id.action_settings:
				Intent goToSettings = new Intent(this,SettingsActivity.class);
				startActivity(goToSettings);
				break;
			default:
				break;
			}
		return super.onOptionsItemSelected(item);		
	}
    
    public JSONArray getContacts() throws JSONException{
		
		String json =null;
		JSONArray listeContactArray;
		HttpGetter httpGetter = new HttpGetter();		
		
		try {
			String urlString = "http://www.alpex-epdm.fr/AM/api/getContactAdmin";			
			URL url = new URL(urlString);
			httpGetter.execute(url);
			json = httpGetter.get();						

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}		
		//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
		if(!json.isEmpty()){		
			listeContactArray = new JSONArray(json);
        return listeContactArray;	
		}
		else{
			//TODO
			//Récupèrer la liste des interimaires en local (SQlite)
			listeContactArray = null;
		return listeContactArray;	
		}			
	}

}
