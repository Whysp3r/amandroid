package com.amandroid.activies;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.IdentityScope;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.amandroid.R;
import com.amandroid.async_task.HttpGetter;
import com.amandroid.database.EmployeDB;
import com.amandroid.database.InterimDB;
import com.amandroid.models.Employe;
import com.amandroid.models.Interim;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.adapter.WorkerAdapter;

public class SelectionInterimActivity extends ListActivity {
	Menu menu;
	
	int nbIntervenants = 0;
	long idChef;
	long idChant;

	ArrayList<Worker> listWorker = new ArrayList<Worker>(); //(Employés)
	ArrayList<Worker> listWorker2 = new ArrayList<Worker>();//(Intérimaires)
	Vector<Interim> interimVector = new Vector<Interim>();//Pour get en local
	
	int loadModel;
	String derniereFiche;
	
	// Search EditText
	 EditText inputSearch;
	 ArrayAdapter<Worker> adapter;

	protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_selectioninterim);
	        
	        //On récupère les paramètress précèdemment
	        Intent intent = getIntent();
	        nbIntervenants = intent.getIntExtra("nbIntervenant", 0);
	        idChef = intent.getLongExtra("idChef", 0);
	        idChant = intent.getLongExtra("idChant", 0); 
	        
	        //On recupère les employés déjà séléctionnés
	        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
	        listWorker = dw.getWorkers();
	        
	        loadModel = intent.getIntExtra("loadModel", 0);
	        getWorker();
	        if(loadModel==1){
				derniereFiche = intent.getStringExtra("derniereFiche");
				generateData();
			}
	        
	        //On appel la fonction pour récupèrer les intérimaires et on peuple la listView via l'adapter
	        adapter = new WorkerAdapter(this,listWorker2);
	        this.setListAdapter(adapter);
	        
	        TextWatcher filterTextWatcher = new TextWatcher() {

	            public void afterTextChanged(Editable s) {
	            }

	            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	            }

	            public void onTextChanged(CharSequence cs, int start, int before, int count) {

	                String searchString = cs.toString();
	                if(searchString.length() < 2) {
	                    adapter = new WorkerAdapter(SelectionInterimActivity.this,listWorker2);
	                    SelectionInterimActivity.this.setListAdapter(adapter);
	                    return;
	                }
	                ArrayList<Worker> workerTemplist = new ArrayList<Worker>();
	                for (int i = 0; i < listWorker2.size(); i++)
	                    {
	                    String name = listWorker2.get(i).getName();
	                    
	                    if (name.toLowerCase().contains(cs))
	                        {
	                            workerTemplist.add(listWorker2.get(i));
	                        }
	                    }
	                adapter = new WorkerAdapter(SelectionInterimActivity.this,workerTemplist);
	                SelectionInterimActivity.this.setListAdapter(adapter);
	            }
	        };
	        inputSearch = (EditText) findViewById(R.id.inputSearch);
	        inputSearch.addTextChangedListener(filterTextWatcher);
	 }
	
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	 
	 private void getWorker() {
		 
		try {
		//Création d'une instance de ma classe EmployeDB
	    InterimDB interimBdd = new InterimDB(this);
	     
	    //On récupère la liste des intérimaires en local
	    interimBdd.open();
		interimVector = interimBdd.getAll();
		interimBdd.close(); 	 
		 
		JSONArray InterimsArray = new JSONArray(); 
		InterimsArray = getInterims();	
		 
		 if(InterimsArray!=null){ 		
		        int nbInterims = InterimsArray.length();
		   	    
		        //On efface les données afin de mettre la DB à jour
		        interimBdd.open();
		        interimBdd.deleteAll();
		        
		        //On peuple la liste des interimaires à partir du json object
		        for(int i=0; i < nbInterims; i++){       
		     
		        	JSONObject interimsObject = InterimsArray.getJSONObject(i);
		        	
		            String nomInterim =  interimsObject.getJSONObject("Employe").getString("nom");	  
		            String prenomInterim =  interimsObject.getJSONObject("Employe").getString("prenom");
		            String idMysql = interimsObject.getJSONObject("Employe").getString("id");	          		        
		            String tel = interimsObject.getJSONObject("Employe").getString("tel");
		            String add = interimsObject.getJSONObject("Employe").getString("adresse");
		            String ville = interimsObject.getJSONObject("Employe").getString("ville");
		            String comm = interimsObject.getJSONObject("Employe").getString("commentaire");
		            String id_agence = interimsObject.getJSONObject("Employe").getString("agence_id");
		            
		            //On met à jour la bdd des intérimaires
		            Interim interim = new Interim(prenomInterim, nomInterim, idMysql, tel, add, ville, comm, id_agence );
		            interimBdd.addInterim(interim);
		            
		            String name = nomInterim+" "+prenomInterim;
		            listWorker2.add(get(name,Long.parseLong(idMysql)));         
		        }
		        interimBdd.close();
			}
		 	else{
	    		for(int j =0; j<interimVector.size(); j++){
			    	   String nameInt = interimVector.get(j).getNom()+" "+interimVector.get(j).getPrenom();
			    	   long id = Long.parseLong( interimVector.get(j).getId_mysql()); 
			    	   listWorker2.add(get(nameInt,id));
				}
			}
	    }
			catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	    
	 }

	 private Worker get(String name, long id) {
		    return new Worker(name, id);
	 }
	 
	 public JSONArray getInterims() throws JSONException{
			
			String json =null;
			JSONArray listeInterimArray;
			HttpGetter httpGetter = new HttpGetter();		
			
			try {
				String urlString = "http://www.alpex-epdm.fr/AM/api/getInterims";			
				URL url = new URL(urlString);
				httpGetter.execute(url);
				json = httpGetter.get();						

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}		
			//Si le flux json est pas vide, on le renvoie sous forme d'un tableau d'objet JSON
			if(!json.isEmpty()){		
				listeInterimArray = new JSONArray(json);
	        return listeInterimArray;	
			}
			else{
				//TODO
				//Récupèrer la liste des interimaires en local (SQlite)
				listeInterimArray = null;
			return listeInterimArray;	
			}			
	}
	public void Valider(View v) {
	        Intent goToRecapWorkerActivity = new Intent (v.getContext(),RecapWorkerActivity.class);
	        ArrayList<Worker> listWorkerToSend = new ArrayList<Worker>();
	        listWorkerToSend.addAll(listWorker);
	        for(int i=0; i<listWorker2.size();i++){
	        	if(listWorker2.get(i).isSelected()){       		
	        		String nameWorker = listWorker2.get(i).getName();
	        		long id = listWorker2.get(i).getId();
	        		listWorkerToSend.add(get(nameWorker,id));
	        	}
	        }
	        //On check si il y a bien tout les intervenant séléctionné
	        if(nbIntervenants == listWorkerToSend.size()){
	        	goToRecapWorkerActivity.putExtra("listWorker", new DataWrapper(listWorkerToSend));
	        	goToRecapWorkerActivity.putExtra("idChef", idChef);
	        	goToRecapWorkerActivity.putExtra("idChant", idChant);
	        	goToRecapWorkerActivity.putExtra("derniereFiche", derniereFiche);
	        	goToRecapWorkerActivity.putExtra("loadModel", loadModel);
	        	
	  		    startActivity(goToRecapWorkerActivity);
	        }
	        else{
	        	new AlertDialog.Builder(this)
			    .setTitle("Nombre d'intevenants incorrecte")
			    .setMessage("Vous avez renseigné "+listWorkerToSend.size()+" intervenants sur "+nbIntervenants)
			    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	//nothing
			        }
			     })
			    .setIcon(android.R.drawable.ic_dialog_alert)
			    .show();
	        } 
	} 
	
	private void generateData(){
    	ArrayList<String> employeSelected = new ArrayList<String>(); 
        try {
        	
        	JSONObject jsonFiche = new JSONObject(derniereFiche);
			JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
			
			for (int i = 0; i < details.length(); ++i) {
				
			    JSONObject detail = details.getJSONObject(i);
			    JSONObject employeDetail = detail.getJSONObject("Employe");
			    employeSelected.add(employeDetail.getString("nom")+" "+employeDetail.getString("prenom"));
			    
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        for (int i = 0; i < listWorker2.size(); i++) {
        	if(employeSelected.contains(listWorker2.get(i).getName())){
        		Worker element = listWorker2.get(i);
        		element.setSelected(true);
        		listWorker2.remove(element);
            	listWorker2.add(0,element);
        	}
		}
    }
}
