package com.amandroid.activies;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amandroid.R;
import com.amandroid.models.Fiche;
import com.amandroid.models.Worker;
import com.amandroid.serializables.DataWrapper;
import com.amandroid.serializables.FicheSerializable;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class RepasGdActivity extends Activity {

	 Menu menu;
	 ArrayList<Fiche> listFiches = new ArrayList<Fiche>();
	 ArrayList<Worker> listWorkers = new ArrayList<Worker>();
     LinearLayout ll ;
     LinearLayout ll2 ;
     int loadModel;
	 String derniereFiche; 
	 
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_repasgd);
	        
	         ll = (LinearLayout) findViewById(R.id.ll);
	         ll2 = (LinearLayout) findViewById(R.id.ll2);
	        
	        //On recupère les employés  séléctionnés
	        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("listWorker");
	        listWorkers = dw.getWorkers();
	        
	        //On recupère les fiches  crées
	        FicheSerializable fs = (FicheSerializable) getIntent().getSerializableExtra("listFiches");
	        listFiches = fs.getFiches();	        
	        
	        ArrayList<String> list = new ArrayList<String>();
	        list.add("Aucun");
	    	list.add("Repas");
	    	list.add("Grand Déplacement");
	    	
	    	loadModel = getIntent().getIntExtra("loadModel", 0);
	        if(loadModel==1){
				derniereFiche = getIntent().getStringExtra("derniereFiche");
			}
	    	
	        //Pour chaque travailleur on ajoute un header et un spinner
	        for(int i=0; i<listWorkers.size(); i++){ 
	        		        	
	        	RelativeLayout rl = new RelativeLayout(this);
	        	rl.setBackgroundColor(getResources().getColor(R.color.header_color));
	        	ll2.addView(rl);
	        	
	        	TextView tv = new TextView(this);
	        	tv.setGravity(Gravity.CENTER);
	        	tv.setTextColor(getResources().getColor(R.color.header_text_color));
	        	tv.setTextSize(16);
	 	        tv.setText(listWorkers.get(i).getName());
	 	        rl.addView(tv);
	 	        	        
	 	        Spinner spinner = new Spinner(this);
	 	        spinner.setId(i);
	 	        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,list);
	 	        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 	        spinner.setAdapter(spinnerArrayAdapter);
	 	        
	 	        if(loadModel==1){
	 	        	int pos = generateData(i);
	 	        	spinner.setSelection(pos);
	 	        }
	 	        
	 	        
	 	        ll2.addView(spinner);	 	      
	 	     	 	     
	 	    }    
	        
	        
	 }
	 
	 public void Valider(View v) {
	     
	     int numberChilds = ll2.getChildCount();
	        for(int i=1; i<numberChilds;){	      
		        int id = ll2.getChildAt(i).getId();		       
		        Spinner spin = (Spinner) findViewById(id);
		        String text = spin.getSelectedItem().toString();
		               
		        if(text =="Aucun"){
		        	listFiches.get(id).setRepas(false);
		        	listFiches.get(id).setGd(false);
		        }
		        
		        if(text =="Repas"){
		        	listFiches.get(id).setRepas(true);
		        	listFiches.get(id).setGd(false);
		        }
		        
		        if(text =="Grand Déplacement"){
		        	listFiches.get(id).setRepas(false);
		        	listFiches.get(id).setGd(true);
		        }		        
		        i=i+2;
	        }
	        Intent goToRecapitulatif = new Intent (v.getContext(),RecapFicheActivity.class);
	        goToRecapitulatif.putExtra("listFiches", new FicheSerializable(listFiches));
	        goToRecapitulatif.putExtra("listWorker", new DataWrapper(listWorkers));
	        startActivity(goToRecapitulatif);
	 } 
	 
	 @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			super.onCreateOptionsMenu(menu);
			this.menu = menu;
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}
	    @Override
		public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {	
				case R.id.action_administration:
					Intent goToAdministration = new Intent(this,AdministrationActivity.class);
					startActivity(goToAdministration);
					break;
				case R.id.action_fiche:
					Intent goToFiche = new Intent(this,LoginActivity.class);
					startActivity(goToFiche);
					break;
				case R.id.action_settings:
					Intent goToSettings = new Intent(this,SettingsActivity.class);
					startActivity(goToSettings);
					break;
				default:
					break;
				}
			return super.onOptionsItemSelected(item);		
		}
	    
	    private int generateData(int index){
			
	        try {
	        	
	        	JSONObject jsonFiche = new JSONObject(derniereFiche);
				JSONArray details =  jsonFiche.getJSONObject("Fiche").getJSONArray("Details");
				
				for (int i = 0; i < details.length(); ++i) {
					
				    JSONObject detail = details.getJSONObject(i);
				    JSONObject employeDetail = detail.getJSONObject("Employe");
				    JSONObject ficheDetail = detail.getJSONObject("Fiche");
				    
				    String name = employeDetail.getString("nom")+" "+employeDetail.getString("prenom");
				    
			    	if(name.equals(listWorkers.get(index).getName())){
			    		
			    		if(ficheDetail.getBoolean("granddeplacement")){
			    			return 2;
			    		}else{
			    			if(ficheDetail.getBoolean("repas")){
			    				return 1;
			    			}else{
			    				return 0;
			    			}
			    		}
			    	}
			        
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        return 0;
	    }

}
