package com.amandroid.serializables;

import java.io.Serializable;
import java.util.ArrayList;


import com.amandroid.models.Worker;

public class DataWrapper implements Serializable {

   private ArrayList<Worker> workers;

   public DataWrapper(ArrayList<Worker> listWorker) {
      this.workers = listWorker;
   }

   public ArrayList<Worker> getWorkers() {
      return this.workers;
   }

}