package com.amandroid.serializables;

import java.io.Serializable;
import java.util.ArrayList;


import com.amandroid.models.Fiche;

public class FicheSerializable implements Serializable {

   private ArrayList<Fiche> fiches;

   public FicheSerializable(ArrayList<Fiche> listFiche) {
      this.fiches = listFiche;
   }

   public ArrayList<Fiche> getFiches() {
      return this.fiches;
   }

}